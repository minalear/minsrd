---
title: 'Traits'
---

## General Traits
* Adaptive - copy another player's trait for the day.
* Artistic - gain advantage on checks where artistic talent or taste is relevant.
* Attractive - gain advantage in situations where good looks might be important.
* Daredevil - gain advantage on checks involving risky or dangerous stunts or maneuvers.
* Fearless - advantage on checks to avoid the effects of fear or intimidation.
* Immunity - gain advantage on checks to avoid poison or disease; medics gain advantage on medical checks when tending to you.
* Keen Senses - advantage when making perception checks or checks involving spotting clues, searching for items, or listening for sounds.
* Lucky - start the game with 2 additional max luck.
* Quick Reflexes - advantage on checks reacting quickly to unexpected situations, such as dodging attacks or avoiding traps.
* Resourceful - advantage when improvising with available resources to solve a problem.

## Skill Traits
* Athlete - advantage on checks relating to feats of strength or agility.
* Animal Magnetism - advantage when handling non-intelligent organisms.
* Master of Disguise - advantage on checks to disguise yourself as someone/something else.
* Tough Negotiator - other characters receive disadvantage to resist your offers when it comes to negotiating
* Gamer - advantage on checks related to some kind of game.
* Grease Monkey - advantage on checks to repair mechanical engines.
* Ghost - terrain type advantage stealth checks.
* Hacker - advantage on checks related to cracking computer or other electrical systems.
* Intimidating - gain advantage on checks when intimidating people into giving you information or complying with your requests.
* Scientist - advantage checks in one field of science.
* Survivalist - advantage on survival related checks within a specific type of environment (desert, arctic, jungle, etc.)

## Combat Traits
* Backstab - when attacking an unaware target in melee, attack with advantage and add +1 to the damage.
* Berserker - enter a blind rage, gaining advantage on melee attacks and +1 damage.  Enemies gain advantage and +1 damage against you.
* Combat Medic - can perform field medicine as a quick action at disadvantage.
* Commander - once per combat as a free action, make a leadership check.  On success, grant a nearby ally who can hear you a free, quick action.
* Crack Shot - +1 damage with chosen category of firearms.
* Deadly Aim - once per scene, you may choose to have your shot ignore the opponent's armor.
* Energetic - once per combat, you can **exert** without taking any Constitution damage.
* Evasion - with a successful dexterity check to take half damage, you suffer zero damage instead.
* Fleet Footed - once per round on your turn, you may take a free action to move up to your speed. Outside of combat, gain advantage on checks involving using your speed.
* Grappler - advantage when attempting to grapple foes.
* Meaty Hands - +1 damage to unarmed attacks.
* Protector - once per combat, you can choose to take damage that would otherwise be dealt to another character in close range to you.
* Rally - once per combat, the character can use a quick action to rally their companions.  Allies heal 1xHD damage and gain advantage on all checks for the next two rounds.  Characters can only benefit from this effect once per combat.
* Shake It Off - once per combat, you can take no actions for an entire round and regain 1xHD health.
* Signature Weapon - choose as specific weapon, gain advantage with attacks with said weapon.  Replace chosen weapon over a month of training.
* Steadied Strikes - +1 damage with melee weapons (excluding fists).
* Thick Skin - +1 health per level.

## Vehicle Traits
* Signature vehicle - gain advantage on checks relating to a specific vehicle.
* Mask Engine - gain advantage on checks to avoid detection by enemy ships.

## Psychic Traits
* Blank - advantage on checks to resist psychic effects.
* Psychic Adept - gain advantage on checks relating to one psychic skill.
* Unhinged Psionic - roll with advantage when rolling on the Torching table.

## Negative Traits
* Addict - choose some form of chemical addiction; disadvantage on all checks if you have not satiated your addiction for the day.
* Alcoholic - advantage on checks to resist negative effects of alcohol; disadvantage on all checks if you have not liquored up for the day.
* Anxious - disadvantage on connect/persuasion checks.
* Claustrophobic - disadvantage on all checks when within enclosed/tight spaces, but gain advantage on checks to escape binds or confinement.
* Gambling Addict - advantage on all gambling checks; cannot pass up the opportunity to gamble.
* Gullible - characters have advantage on checks to lie to you.
* Insomniac - must make a constitution check when sleeping to gain the benefits of sleep; receive advantage on checks that require intense concentration or focus.
* Phobia - disadvantage on checks when a specified phobia is present (e.g. fear of heights, xenophobic, etc.)
* Paranoid - others attempting to aid or support you are at disadvantage on their check to do so.
* Pacifist - 2x disadvantage on all combat related checks and avoids conflict at all possibilities.  Advantage on checks to end conflicts peacefully or to avoid them altogether.

## Trait Ideas
* Night Owl - disadvantage on all checks performed during the day in open daylight or in bright light; advantage on all checks when performed at night or in the dark.