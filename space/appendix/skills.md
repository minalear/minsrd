---
title: 'Skills'
---

## General Skills

| Skill | Ability | Description | 
|---|---|---|
| Admin | INT/CHA/WIS | Manage an organization, handle paperwork, analyze records, and keep an institution functioning on a daily basis. Roll it for bureaucratic expertise, organizational management, legal knowledge, dealing with government agencies, and understanding how institutions really work. |
| Athletics | STR/DEX | Apply trained speed, strength, or stamina in some feat of physical exertion. Roll it to run, jump, lift, swim, climb, throw, and so forth. You can use it as a combat skill when throwing things, though it doesn’t qualify as a combat skill for other ends. ||
| Craft | ANY | |
| Deception | CHA/WIS | |
| Disable Device | DEX/INT | |
| Fix | INT/WIS | Create and repair devices both simple and complex. How complex will depend on your character’s background; a lostworlder blacksmith is going to need some study time before he’s ready to fix that broken fusion reactor, though he can do it eventually. Roll it to fix things, build things, and identify what something is supposed to do. |
| Intimidate | CHA/STR | |
| Investigate | INT/WIS | |
| Know | INT | Know facts about academic or scientific fields. Roll it to understand planetary ecologies, remember relevant history, solve science mysteries, and know the basic facts about rare or esoteric topics. |
| Medicine | INT/WIS | Employ medical and psychological treatment for the injured or disturbed. Roll it to cure diseases, stabilize the critically injured, treat psychological disorders, or diagnose illnesses. |
| Notice | WIS | Spot anomalies or interesting facts about your environment. Roll it for searching places, detecting ambushes, spotting things, and reading the emotional state of other people. |
| Perform | CHA/INT | Exhibit some performative skill. Roll it to dance, sing, orate, act, or otherwise put on a convincing or emotionally moving performance. |
| Persuasion | CHA/WIS/INT | Convince other people of the facts you want them to believe. What they do with that conviction may not be completely predictable. Roll it to persuade, charm, or manipulate others in conversation. |
| Pilot | INT/WIS/DEX | Use this skill to pilot vehicles or ride beasts. Roll it to fly spaceships, drive vehicles, ride animals, or tend to basic vehicle repair. This skill doesn’t help you with things entirely outside the scope of your background or experience, though with some practice a PC can expand their expertise. |
| Program | INT | Operating or hacking computing and communications hardware. Roll it to program or hack computers, control computer-operated hardware, operate communications tech, or decrypt things. |
| Sense Motive | WIS | |
| Sneak | DEX | Move without drawing notice. Roll it for stealth, disguise, infiltration, manual legerdemain, pickpocketing, and the defeat of security measures. |
| Steal | DEX | |
| Survive | WIS/INT | Obtain the basics of food, water, and shelter in hostile environments, along with avoiding their natural perils. Roll it to handle animals, navigate difficult terrain, scrounge urban resources, make basic tools, and avoid wild beasts or gangs. |
| Trade | INT/CHA | Find what you need on the market and sell what you have. Roll it to sell or buy things, figure out where to purchase hard-to-get or illicit goods, deal with customs agents, or run a business. |
| Work | ANY | This is a catch-all skill for professions not represented by other skills. Roll it to work at a particular profession, art, or trade. |

## Combat Skills

| Skill | Ability | Description |
|---|---|---|
| Grapple | STR/DEX | |
| Punch | STR/DEX | Use it as a combat skill when fighting unarmed. If your PC means to make a habit of this rather than as a recourse of desperation, you should take the Unarmed Fighter focus described later. |
| Shoot | DEX | Use it as a combat skill when using ranged weaponry, whether hurled rocks, bows, laser pistols, combat rifles, or ship’s gunnery. |
| Melee | STR/DEX | Use it as a combat skill when wielding melee weapons, whether primitive or complex. |

## Psychic Skills

| Skill | Ability | Description | 
|---|---|---|
| Biosionics | INT/CHA | Biopsionic powers repair, augment, debilitate, or damage living creatures. \[See [Biopsionics](/space/rules/psionics#Biopsionics)\] |
| Metasionics | INT/CHA | Metapsionics control and modify psychic energy itself, amplifying other psionic abilities. \[See [Metasionics](/space/rules/psionics#Metasionics)\] |
| Precognition | INT/CHA | Precognition powers relate to sensing the cascade of future events and reading the achronal chaos emanating from the psychic's brain. \[See [Precognition](/space/rules/psionics#Precognition)\] |
| Telekinesis | INT/CHA | Telekinesis can be used to manipulate matter and the surrounding environment. \[See [Telekinesis](/space/rules/psionics#Telekinesis)\] |
| Telepathy | INT/CHA | Telepathy is used to read or modify other's thoughts or memories. \[See [Telepathy](/#Telepathy)\] |
| Teleportation | INT/CHA | Teleportation is used to transport instantaneously between points in space. \[See [Teleportation](/space/rules/psionics#Teleportation)\] |

