---
title: 'Armor'
---

| Type | Description | Effect |
|-------------|---|---|
| **Ballistic**   | Standard issue flak jacket armored used by basic infantry and security personnel.  Provides basic protection against small arms fire and shrapnel, but little to no protection against energy-based weapons. | 1x use |
| **Composite**   | Armor using layers of advanced materials, ranging from carbon fiber to plasteel, typically used by special forces. | 2x use<br>Useable against laser |
| **Power Armor** | Rare and expensive armor that utilizes thick plating on top of a powered exoskeleton that provides enhanced strength and mobility. Comes in two varieties; internally powered and externally powered.  Externally powered armor requires an external power source, typically in the form of a power pack. | 5x use<br>Increase strength and dexterity by 5<br>Increase base speed by 5 |
| **Nanotech**    | Advanced armor designed after xeno specifications that utilize millions of micromachines to rapidly repair and regenerate materials. | 5x use |
| **Energy**      | Advanced armor that utilizes experimental phasing technology to produce a field that can negate any attack by diverting them into the Warp.  Requires an external power source. | Infinite uses while charged |

## Upgrade Components
Base armors can be upgrades with components to give them additional effects or bonuses. 

| Component | Description | Effect |
|---|---|---|
| VAC Suit | Pressurized vacuum suit that allows traversal into space or potentially other hazardous environments |  |
| Chameleon | Color changing to allow subtle blending into background scenes; note, this is not active camo | Advantage on stealth checks to avoid being seen |
| Active Camo | Suit actively hides user from sight using advanced nanofiber technology.  More advanced than chameleon blending. | Immune to vision based effects |
| Jump Pack | | |
