---
title: 'Backgrounds'
---

|  | Background |
|---|---|
| 1 | **Barbarian**: born of a primitive world |
| 2 | **Clergy**: a consecrated mana or woman |
| 3 | **Criminal**: thief, rogue, liar, or worse |
| 4 | **Dilettante**: with money if not purpose |
| 5 | **Entertainer**: artful and beguiling |
| 6 | **Merchant**: whether peddler or far trader |
| 7 | **Noble**: by blood or by social capital |
| 8 | **Official**: a functionary of some greater state |
| 9 | **Peasant**: whether primitive or high-tech |
| 10 | **Physician**: a healer of the sick and maimed |
| 11 | **Pilot**: rider, sailor, or vehicle-driver |
| 12 | **Politician**: aspiring to leadership or control |
| 13 | **Scholar**: a scientist or academic |
| 14 | **Soldier**: mercenary, conscript, career military |
| 15 | **Spacer**: dwelling in the deep-space habs |
| 16 | **Technician**: artisan, engineer, or builder |
| 17 | **Thug**: ruffian, strong arm |
| 18 | **Vagabond**: roaming without a home |
| 19 | **Worker**: a cube drone or day laborer |

## Barbarian
Standards of barbarism vary when many worlds are capable of interstellar spaceflight, but your hero comes from a savage world of low technology and high violence. Their planet may have survived an all-consuming war, or been deprived of critical materials or energy resources, or simply have been colonized by confirmed Luddites. Other barbarians might be drawn from the impoverished underclass of advanced worlds or the technologically-degenerate inheritors of some high-tech space station or planetary hab.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Survive-0        | 1  | +1 Any Stat | 1  | Any Combat |
|                  | 2  | +2 Physical | 2  | Connect |
| **Quick Skills** | 3  | +2 Physical | 3  | Athletics |
| Survive-0        | 4  | +2 Mental   | 4  | Persuasion |
| Notice-0         | 5  | Athletics   | 5  | Notice |
| Any Combat-0     | 6  | Any Skill   | 6  | Punch |
|                  |    |             | 7  | Sneak |
|                  |    |             | 8  | Survive |

## Clergy
Faith is nigh-universal among human civilizations, and your hero is dedicated to one such belief. Some clergy are conventional priests or priestesses, while others might be cloistered monastics or nuns, or more martial warrior-monks. Modern-day faiths such as Christianity, Islam, Judaism, Hinduism, Buddhism, and other creeds all exist in various sectors, often in altered form, while some worlds have developed entirely new deities or faiths. If you’d like to create your own religion, you can work with the GM to define its characteristic beliefs.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Persuasion-0     | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Connect |
| **Quick Skills** | 3  | +2 Physical | 3  | Know |
| Persuasion-0     | 4  | +2 Mental   | 4  | Persuasion |
| Perform-0        | 5  | Connect     | 5  | Notice |
| Know-0           | 6  | Any Skill   | 6  | Perform |
|                  |    |             | 7  | Sense Motive |
|                  |    |             | 8  | Persuasion |

## Criminal
Whether thief, murderer, forger, smuggler, spy, or some other variety of malefactor, your hero was a criminal. Some such rogues are guilty only of crossing some oppressive government or offending a planetary lord, while others have done things that no civilized society could tolerate. Still, their ability to deal with the most desperate and dangerous of contacts and navigate the perils of a less-than-legal adventure can make them attractive associates for a party of freebooters bent on profit and glory more than strict legality.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Sneak-0          | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Any Combat |
| **Quick Skills** | 3  | +2 Physical | 3  | Connect |
| Sneak-0          | 4  | +2 Mental   | 4  | Notice |
| Connect-0        | 5  | Connect     | 5  | Program |
| Steal-0          | 6  | Any Skill   | 6  | Sneak |
|                  |    |             | 7  | Steal |
|                  |    |             | 8  | Trade |

## Dilettante
Your hero never had a profession, strictly speaking, but spent their formative years in travel, socializing, and a series of engaging hobbies. They might have been the scion of a wealthy industrialist, a planetary noble’s younger offspring, or a hanger-on to someone with the money and influence they lacked. By the time your hero’s adventures start, they’ve run through the money that once fueled their lifestyle. Extreme measures may be necessary to acquire further funding.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Connect-0        | 1  | +1 Any Stat | 1  | Any Skill |
|                  | 2  | +1 Any Stat | 2  | Any Skill |
| **Quick Skills** | 3  | +1 Any Stat | 3  | Connect |
| Connect-0        | 4  | +1 Any Stat | 4  | Know |
| Know-0           | 5  | Connect     | 5  | Perform |
| Persuasion-0     | 6  | Any Skill   | 6  | Pilot |
|                  |    |             | 7  | Persuasion |
|                  |    |             | 8  | Trade |

## Entertainer
Singers, dancers, actors, poets, writers... the interstellar reaches teem with artists of unnumbered styles and mediums, some of which are only physically possible with advanced technological support. Your hero was a dedicated entertainer, one likely focused in a particular form of art. Patrons and talent scouts can be temperamental, however, and sometimes a budding artist needs to take steps to find their audience. Or at least, to find their audience’s money

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Perform-0        | 1  | +1 Any Stat | 1  | Any Combat |
|                  | 2  | +2 Mental   | 2  | Connect |
| **Quick Skills** | 3  | +2 Mental   | 3  | Athletics |
| Perform-0        | 4  | +2 Physical | 4  | Sense Motive |
| Persuasion-0     | 5  | Connect     | 5  | Perform |
| Connect-0        | 6  | Any Skill   | 6  | Perform |
|                  |    |             | 7  | Sneak |
|                  |    |             | 8  | Persuasion |

## Merchant
Your hero was or is a trader. Some merchants are mere peddlers and shopkeepers on primitive, lowtech worlds, while others are daring far traders who venture to distant worlds to bring home their alien treasures. The nature of trade varies widely among worlds. On some of them, it’s a business of soberly-dressed men and women ticking off trades on virtual terminals, while on others it is a more... active pursuit, requiring the judicious application of monoblades and deniable gunfire against competitors. Sometimes a deal goes bad or capital needs to be raised, and a merchant’s natural talents are turned toward the perils of adventure.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Trade-0          | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Any Combat |
| **Quick Skills** | 3  | +2 Mental   | 3  | Connect |
| Trade-0          | 4  | +2 Mental   | 4  | Fix |
| Persuasion-0     | 5  | Connect     | 5  | Know |
| Connect-0        | 6  | Any Skill   | 6  | Notice |
|                  |    |             | 7  | Trade |
|                  |    |             | 8  | Persuasion |

## Noble
Many planets are ruled by a class of nobles, and your hero was a member of one such exalted group. Such planets are often worlds of exquisite courtesy alloyed with utterly remorseless violence, and a misplaced word at the morning levee can result in an executioner’s monoblade at noon. Your hero has done something or been the victim of something to dislodge them from their comfortable place at court. Without their familiar allies, wealth, or influence, they must take a new place in the world, however distasteful that claiming might be.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Persuasion-0     | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Any Combat |
| **Quick Skills** | 3  | +2 Mental   | 3  | Connect |
| Persuasion-0     | 4  | +2 Mental   | 4  | Know |
| Connect-0        | 5  | Connect     | 5  | Persuasion |
| Admin-0          | 6  | Any Skill   | 6  | Notice |
|                  |    |             | 7  | Pilot |
|                  |    |             | 8  | Sense Motive |

## Official
Most advanced worlds run on their bureaucracies, the legions of faceless men and women who fill unnumbered roles in keeping the government running as it should. Your hero was one such official. Some were law enforcement officers, others government office clerks or tax officials or trade inspectors. However necessary the work may be, it is often of unendurably tedious nature, and any man or woman with an adventurous spark to their blood will soon find themselves desperate for more exciting use of their talents.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Admin-0          | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Any Skill |
| **Quick Skills** | 3  | +2 Mental   | 3  | Connect |
| Admin-0          | 4  | +2 Mental   | 4  | Know |
| Persuasion-0     | 5  | Connect     | 5  | Sense Motive |
| Connect-0        | 6  | Any Skill   | 6  | Notice |
|                  |    |             | 7  | Persuasion |
|                  |    |             | 8  | Trade |

## Peasant
A technologically-advanced world can usually produce all its necessary foodstuffs and basic resources with a handful of workers, the bulk of the labor being performed by agricultural bots. On more primitive worlds, or those with a natural environment that requires close personal attention to crops, a class of peasants will emerge. These men and women often become chattel, part and parcel of the land they occupy and traded among their betters like the farm equipment of richer worlds. Your hero was not satisfied with that life, and has done something to break free from their muddy and toilsome past.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Athletics-0      | 1  | +1 Any Stat | 1  | Connect |
|                  | 2  | +2 Physical | 2  | Athletics |
| **Quick Skills** | 3  | +2 Physical | 3  | Fix |
| Athletics-0      | 4  | +2 Physical | 4  | Notice |
| Sneak-0          | 5  | Athletics   | 5  | Sneak |
| Survive-0        | 6  | Any Skill   | 6  | Survive |
|                  |    |             | 7  | Trade |
|                  |    |             | 8  | Work |

## Physician
Bodies wear and break, even on worlds that possess the full resources of advanced postech medicine. Your hero was a physician, one trained to cure the maladies of the body or the afflictions of the mind. Some physicians are conventional health workers, while others are ship’s surgeons, military medics, missionary healers of an expanding faith, or dubious slum doctors who’ll graft over laser burns with no awkward questions asked. Wherever men and women go into danger, however, the skills of a physician are eagerly sought.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Heal-0           | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Physical | 2  | Connect |
| **Quick Skills** | 3  | +2 Mental   | 3  | Fix |
| Heal-0           | 4  | +2 Mental   | 4  | Heal |
| Know-0           | 5  | Connect     | 5  | Know |
| Notice-0         | 6  | Any Skill   | 6  | Notice |
|                  |    |             | 7  | Persuasion |
|                  |    |             | 8  | Trade |

## Pilot
A pilot’s role is a broad one in the far future. The most glamorous and talented navigate starships through the metadimensional storms of interstellar space, while less admired figures fly the innumerable intra-system shuttles and atmosphere craft that serve in most advanced systems. On other worlds, this career might reflect a long-haul trucker, or a horse-riding messenger, or an intrepid sailor on an alien sea. As the Pilot skill covers all these modes of transport, any character whose role revolves around vehicles or riding beasts might justify their selection of this career.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Pilot-0          | 1  | +1 Any Stat | 1  | Connect |
|                  | 2  | +2 Physical | 2  | Athletics |
| **Quick Skills** | 3  | +2 Physical | 3  | Fix |
| Pilot-0          | 4  | +2 Mental   | 4  | Notice |
| Fix-0            | 5  | Connect     | 5  | Pilot |
| Shoot or Trade-0 | 6  | Any Skill   | 6  | Pilot |
|                  |    |             | 7  | Shoot |
|                  |    |             | 8  | Trade |

## Politician
The nature of a political career varies from world to world. On some, it’s much like one we’d recognize, with glad-handing voters, loud rallies, and quiet back room deals with supposed rivals in government. On others, it might involve a great deal more ceremonial combat, appeals to councils of elders, and success at ritual trials. Whatever the details, your hero was a politician in their home culture. Something went wrong, though, and the only way to fix it is to get clear of your constituents for a while and seek some alternative means of advancement.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Persuasion-0     | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Connect |
| **Quick Skills** | 3  | +2 Mental   | 3  | Connect |
| Persuasion-0     | 4  | +2 Mental   | 4  | Sense Motive |
| Sense Motive-0   | 5  | Connect     | 5  | Notice |
| Connect-0        | 6  | Any Skill   | 6  | Perform |
|                  |    |             | 7  | Persuasion |
|                  |    |             | 8  | Persuasion |

## Scholar
Scientists, sages, and professors all qualify under this career. Your hero was one such, a man or woman with a life dedicated to knowledge and understanding. It might have involved the technical expertise of a metadimensional structures engineer or the sacred memorization of the chronicles of some lostworlder sage-order, but your hero’s life was in learning. Sometimes that learning cannot be found in familiar surroundings, however, and for one reason or another, willing or not, your hero must venture out into the wider world.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Know-0           | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Mental   | 2  | Connect |
| **Quick Skills** | 3  | +2 Mental   | 3  | Fix |
| Know-0           | 4  | +2 Mental   | 4  | Know |
| Connect-0        | 5  | Connect     | 5  | Notice |
| Admin-0          | 6  | Any Skill   | 6  | Perform |
|                  |    |             | 7  | Program |
|                  |    |             | 8  | Persuasion |

## Soldier
Whatever the technology or structure of their parent world, a soldier’s work is universal. Your hero was a professional fighter, whether that took the form of a barbarian noble’s thegn, a faceless conscript in a planetary army, or an elite soldier in the service of a megacorp’s private military. Whether it was because they were on the losing side, choosing to leave the service, or being forced to flee a cause they couldn’t fight for, they now find themselves navigating a world where their most salable skill is one that can cause them a great deal of trouble.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Any Combat-0     | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Physical | 2  | Any Combat |
| **Quick Skills** | 3  | +2 Physical | 3  | Athletics |
| Any Combat-0     | 4  | +2 Physical | 4  | Fix |
| Athletics-0      | 5  | Athletics   | 5  | Persuasion |
| Survive-0        | 6  | Any Skill   | 6  | Notice |
|                  |    |             | 7  | Sneak |
|                  |    |             | 8  | Survive |

## Spacer
Almost every advanced world is highly dependent upon the resources that space flight brings them. Some of this work can be automated, but every really important task needs a flexible human operator to oversee the work. Your hero is one such spacer, either a worker who toils in the sky or a native voidborn man or woman who has spent their entire life outside of natural gravity. It’s not uncommon for such workers to find better prospects in places where they can breathe without a vacc suit.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Fix-0            | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Physical | 2  | Connect |
| **Quick Skills** | 3  | +2 Physical | 3  | Athletics |
| Fix-0            | 4  | +2 Mental   | 4  | Fix |
| Pilot-0          | 5  | Athletics   | 5  | Know |
| Program-0        | 6  | Any Skill   | 6  | Pilot |
|                  |    |             | 7  | Program |
|                  |    |             | 8  | Persuasion |

## Technician
Old things break and new things need to be made.  Whether a humble lostworlder blacksmith or an erudite astronautic engineer, your hero made a career out of building and mending the fruits of technology.  While almost every society has a need for such services, not all of them treat their providers as generously as a technician might wish. Sometimes, these same talents can be turned toward less licit ends, and a skilled technician’s expertise is always useful to an adventuring group that plans to rely on anything more sophisticated than a sharpened stick.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Fix-0            | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Physical | 2  | Connect |
| **Quick Skills** | 3  | +2 Mental   | 3  | Athletics |
| Fix-0            | 4  | +2 Mental   | 4  | Fix |
| Athletics-0      | 5  | Connect     | 5  | Fix |
| Notice-0         | 6  | Any Skill   | 6  | Know |
|                  |    |             | 7  | Notice |
|                  |    |             | 8  | Pilot |

## Thug
Your hero was a bruiser. They might have had a notional allegiance to some so-called “army”, or have been part of some crime boss’ strong-arm crew, or simply been a private contractor of misfortune for those who failed to pay up. They might have even been a fist in a righteous cause, defending their neighborhood from hostile outsiders or serving as informal muscle for a local leader in need of protection. Whatever the details, they’ve had to move on from their old life, and their new one is likely to involve a similar application of directed force.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Any Combat-0     | 1  | +1 Any Stat | 1  | Any Combat |
|                  | 2  | +2 Mental   | 2  | Connect |
| **Quick Skills** | 3  | +2 Physical | 3  | Athletics |
| Any Combat-0     | 4  | +2 Physical | 4  | Notice |
| Persuasion-0     | 5  | Connect     | 5  | Sneak |
| Connect-0        | 6  | Any Skill   | 6  | Stab or Shoot |
|                  |    |             | 7  | Survive |
|                  |    |             | 8  | Persuasion |

## Vagabond
A dilettante has money and friends; your hero simply has the road. Whether they were knocked loose from polite society at a young age or have recently found themselves cast out of a familiar life, they now roam the ways of the world and the spaces between. Some heroes find this life satisfying, with its constant novelty and the regular excitement of bare survival. Others long for a more stable arrangement, and are willing to lend their pragmatic talents to a group that offers some prospect of profit.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Survive-0        | 1  | +1 Any Stat | 1  | Any Combat |
|                  | 2  | +2 Physical | 2  | Connect |
| **Quick Skills** | 3  | +2 Physical | 3  | Notice |
| Survive-0        | 4  | +2 Mental   | 4  | Perform |
| Sneak-0          | 5  | Athletics   | 5  | Pilot |
| Notice-0         | 6  | Any Skill   | 6  | Sneak |
|                  |    |             | 7  | Survive |
|                  |    |             | 8  | Work |

## Worker
Countless in number, every industrialized world has swarms of workers to operate the machines and perform the labor that keeps society functioning.  Cooks, factory laborers, mine workers, personal servants, lawyers, clerks, and innumerable other roles are covered under this career. If your hero rolls or picks Work as a skill but has a career that would better fit another existing skill, they may substitute it accordingly. Thus, a wage-slave programmer might take Program instead of Work, while a lawyer would use Admin instead as a reflection of their litigious talent.

| Free Skill       | d6 | Growth      | d8 | Learning   |
|------------------|----|-------------|----|------------|
| Work-0           | 1  | +1 Any Stat | 1  | Admin |
|                  | 2  | +2 Any Stat | 2  | Any Skill |
| **Quick Skills** | 3  | +2 Any Stat | 3  | Connect |
| Work-0           | 4  | +2 Any Stat | 4  | Athletics |
| Athletics-0      | 5  | Athletics   | 5  | Fix |
| Connect-0        | 6  | Any Skill   | 6  | Pilot |
|                  |    |             | 7  | Program |
|                  |    |             | 8  | Work |