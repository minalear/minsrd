---
title: 'Ideas'
weight: -1
---

List of miscellaneous ideas and todos that may or may not be incorporated into the ruleset.

* Super advantage/disadvantage?  Could allow stacking advantages or disadvantages, each time adding an additional d6 to the roll.
  * Could refer to Advantage/Disadvantage as "Advantage Dice" where "double advantage" means you add 2d6 to the roll
* More actions in combat;
  * Dodge, Brace
* Cover?
  * Soft cover is disadvantage, hard cover is double disadvantage
* Cybernetics
* Contested Checks
* Fate die - players roll a d6 before the session and can replace a single d6 with the one they rolled (theirs, another player's or NPC)

## Action Definition
* Execution Action
* Move Action
* On Turn Action

## Todo
* Disadvantage on shooting attacks while engaged in melee
* Shock damage on weapons
* Ship Combat

## Combat
* Initiative system
  * Separate initiatives for both groups; alternating between groups
  * Dex or Wis check to determine initiative order