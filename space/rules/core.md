---
title: 'Core Rules'
---

## Checks

Whenever your character attempts any action with a chance of failure, they must perform a **Check**.

1. Roll two six-sided dice (2d6).
1. Add any relevant modifiers (for things like abilities, skills, difficulty, and circumstances).
1. If the result equals or exceeds 8, the action succeeds.  If the result is lower than 8, the action fails.

### Advantage and Disadvantage

Sometimes checks may be advantaged or disadvantaged.  One advantage and one disadvantage cancels each other out.  For each advantage (or disadvantage), add an extra d6 to the check.  If you have advantage, take the highest two die.  If you have disadvantage, take the lowest two die.

*example: Courtney is attempting to assassinate a target with  her sniper from 4 km away.  She is using her signature weapon, which grants advantage on attacks.  She also has advantage on long range attacks.  Zack is also spotting for her, granting a third advantage to the shot.  However, the desert winds is kicking up a lot of sand, providing cover for the target, giving her disadvantage.  One of her advantages is canceled by the disadvantage, but she still has two advantages to the check.  She rolls 4d6 and takes the highest of the two.*

### Impact

Sometimes successful checks can have additional effects measured in **Impact**.  A check has an Impact equal to the total result minus 7.  Impact is mainly used for damage in combat, but can also be used to represent complex, time-consuming tasks (see [Cumulative Checks](#cumulative-checks)).

*example: Zack makes a skill check and rolls a 6 and a 4 and has a modifier of +2.  The Impact of the roll would be 5. (6 + 4 + 2) - 7.*

### Cumulative Checks

Occasionally a task is more complex and requires a series of checks.  These are typically handled using **Cumulative Checks** and use **Impact** to resolve.

*example: Ryan attempts to hack the ship's system and eject Caleb out of an airlock.  The system is quite complex, so the GM requires a cumulative Impact of 3.  He makes a skill check and rolls a 5 and a 3, totalling 8.  The check is a success, but Ryan has only accrued a total of 1 Impact (8 - 7 = 1).  He must continue making checks until the accrued total is 3.  This allows Caleb the chance to perform checks to escape this scenario.*

### Saving Throws

A special kind of check is a **Saving Throw** which typically is required in response to some outside event, like dodging a trap or resisting poison.  These checks are made using your base ability modifier plus any miscellaneous modifiers.

*example: Zack has been afflicted with Ligma, a terrible disease from the planet Balls.  To resist the effects, he must make Constitution saving throw.  His Con modifier is +1 and he has a trait giving him an additional +1.  He rolls a 3 and a 1, totalling 6 (3 + 1 + 1 + 1 = 6).  He fails his Saving Throw and suffers the penalties of Ligma.*

## Skills

PCs have skills in specific areas of expertise.  Players can have skills ranking between 0 and 4, with 0 being basic competence and 4 being exceptional prowess.  Whenever a PC makes a skill check, they add their level within that skill in addition to their relevant ability modifier.

For complex tasks, a PC will automatically fail if they are not trained in the skill at all.  A PC without any ranks of Medic simply cannot perform brain surgery without killing the patient.

For less complex tasks, such as melee combat, a PC will suffer disadvantage for making any unskilled checks.

A player's skills cannot exceed half their player level (rounded down).  This can be found on the [character advancement chart](/space/rules/character-advancement.md).

| Skill | Capabilities |
|---|---|
| 0 | Basic competence in the skill, such as an ordinary practitioner would have |
| 1 | An experienced professional in the skill, clearly better than most |
| 2 | Veteran expert, one respected even by those with considerable experience |
| 3 | Master of the skill, likely one of the best on the planet |
| 4 | Superlative expertise, one of the best in the entire stellar sector |

A list of skills can be found [here](/space/appendix/skills.md).

## Luck

PCs have a resource called **Luck**.  Players track their current and maximum luck and typically begin the game with 10 luck.

After making a check, a player may spend 1 point of their luck to re-roll 1 of the dice, using the new total for the throw.  They make keep re-rolling, spending 1 luck each time.  If a re-rolled die shows a value of 1, the check automatically fails.  The action goes wrong in the worst possible way and the PC reduces their maximum luck to the level of their current luck.

PCs regain luck in safety during downtime, like at port or on board their ship.  If a PC doesn't use any luck for 2 weeks, they regain 1 luck, up to their current maximum.

## Conditions

Bad things can happen to PCs.  They might get drunk, poisoned, or get a broken leg.  These are **Conditions** and can negatively impact checks.  Conditions come with a severity like *Nauseous 3*.  Subtract the severity of a PC's worst condition to any check where it is an impediment.  For example, if a PC has the condition "leg wound 2", they must subtract 2 from any checks to run, jump, climb, etc.