---
title: 'Character Creation'
---

## Outline

1. Determine Ability Scores
1. Pick a Background
1. Pick a Trait
1. Pick a Class
1. Pick a Focus
1. Determine starting health and pick additional skills
1. Get equipped

## Ability Scores

**Ability Scores** represent the character's most basic attributes.  They are their raw talent and prowess.

**Strength (STR)** \
A character's physical strength, fitness, and forcefulness.

**Dexterity (DEX)** \
A character's physical coordination, agility, and reflexes.

**Constitution (CON)** \
A character's ability to sustain damage, stamina, and determination.

**Intelligence (INT)** \
A character's intellect and quickness of mind.

**Wisdom (WIS)** \
A character's attention to detail, judgments, reading situations, and intuition.

**Charisma (CHA)** \
A character's force of will, charm, and presence.

### Generating Ability Scores

There are a number of different methods used to generate ability scores.  Each of these methods gives a different level of flexibility and randomness to character generation.

**Standard**: Roll 3d6 and record this total.  Repeat this process five times and assign these totals to ability scores as you see fit.  You may then change one ability of your choice to 14 if you wish.

**Heroic**: Roll 4d6, discard the lowest die result, and record this total.  Repeat this process five times and assign these totals to ability scores as you see fit.  This method is less random than Standard and tends to create characters with above average ability scores.

**Standard Array**: You may assign the following scores to your abilities in any order you wish: 14, 12, 11, 10, 9, and 7.

Once you've assigned your abilities, record the modifier for each of them.  This modifier is applied to relevant skill checks or other rolls related to the ability.  Ability scores cannot be greater than 18 for player characters.

### Ability Modifiers

| Score | Modifier |
| --- | --- |
| 3 | \-2 |
| 4 - 7 | \-1 |
| 8 - 13 | 0 |
| 14 - 17 | +1 |
| 18 | +2 |

## Background

**Backgrounds** represent skills and abilities that your character would have acquired prior to starting the game.  A background is simply a thumbnail description of the kind of life your character lived before they took up adventuring.  It may not have been the only thing they did with their life, but it's the thing that taught them most of their existing skills.

Each background either gives three starting skills or two starting skills and an ability score boost.  If none of the backgrounds appeal to you, you can simply describe your hero's prior life to the GM and pick any three skills (or two skills and a +2 to an ability score) that fit that past.

Once you pick a background, your character gains several skills associated with their past.  First, you get the free skill associated with the background.  Next, you may pick one option from the **Growth** table and one from the **Learning** table.  Or, alternatively, you may roll twice between any of the two tables.  If you pick skills, you may pick the same skill twice if you wish, to improve its starting proficiency.

Occasionally, the GM may have you roll a *Background Check* whenever your background is relevant to a particular task, but you lack the required skill.  The bonus for this check is half your character level, rounded down.

Backgrounds can be found [here](/space/appendix/backgrounds.md).

## Traits

**Traits** are unique features of competent and driven characters, such as player characters.  Each character typically begins the game with one Trait.

Traits can be found [here](/space/appendix/traits.md).

## Class

Your **Class** represents your hero's best set of tools for dealing with their problems.  They're the skills and capabilities that they're most likely to turn to to resolve a problem, but not their only tools.  A Psychic can still shoot a gun and a Warrior can try to fast-talk a mark.

There are four possible classes you can choose for your hero: Adventurer, Expert, Psychic, and Warrior.  Experts are PCs that rely on non-combat skills such as stealth, persuasion, technical expertise, piloting skills, vast learning, or personal connections.  Psychics are those rare beings granted the ability to bend space and time to their whim; able to perform feats of telekinesis, precognition, telepath, and other psionic disciplines.  Warriors are hardened combatants, skilled in the arts of war and violent conflict, and Adventurers include those with a mix of talents and abilities, ones who don't fit well into one of the other classes.

In-depth class descriptions can be found [here](/space/appendix/classes.md).

## Focus

Every member of a class has certain baseline abilities that let them do well at their role. Warriors are always good combatants, Experts are always gifted with their skills, and Psychics always have a few pertinent psionic abilities. Aside from these baseline abilities, characters also have a **Focus**.

A focus is an additional knack, perk, or aptitude that a hero has, one that grants them certain benefits in play. Each focus has one or more levels of increasingly-strong effect.

Beginning PCs can pick one level of focus.  PCs with the Expert class or the Partial Expert Adventurer class option can choose an additional level in any non-combat focus related to their background, including level 2 in their initial choice.

As your hero advances in experience, you'll get the opportunity to pick up new focuses or improve existing ones as you refine your talents or learn new tricks.  In some cases, you might work with the GM to come up with a custom focus reflecting your PC's specific special talent or a perk that makes sense given their recent adventures.

Focuses can be found [here](/space/appendix/focuses.md)

## Additional Skills and Health

Players gain additional skill ranks equal to their Intelligence or Wisdom modifiers (whichever is highest).

Player's starting health is equal to 4 + their CON modifier.