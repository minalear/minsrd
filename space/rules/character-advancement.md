---
title: 'Character Advancement'
---

| Level | Progression | Max Skill Level |
|---|---|---|
| 1 | Background, 2 Traits, Class, Focus | 1 | 
| 2 | +1 Ability Score | 1 |
| 3 | +1 Skill, +1 Focus | 2 |
| 4 | +1 Ability Score | 2 |
| 5 | +1 Skill, +1 Focus | 3 |
| 6 | +1 Ability Score | 3 |
| 7 | +1 Skill, +1 Focus | 4 |
| 8 | +1 Ability Score | 4 |