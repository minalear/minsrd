---
title: 'Combat'
---

When a fight is simple, resolve it with a single check.  If the outcome is a foregone conclusion, just say what happens.  For more complex skirmishes, use these rules.

## Turns and Rounds

Combat is separated into **turns** and **rounds**.  Each character takes **one turn per round** unless otherwise specified.  A round ends when every character capable of taking a turn has taken a turn.

Turns represent activity, not the passing of time.  Even though characters act in a certain order, the turns in each round are assumed to narratively occur at roughly the same time.

Players always get to act first.  When combat begins, the players agree on which player (or allied NPC) to take the first turn.  If the players can't agree on someone then the DM chooses.

Next, the DM chooses a hostile NPC to act, followed by a player (or allied NPC) nominated by the player who acted previously.  This is followed by another hostile NPC of the DM's choice, then another player, and so on, alternating between hostile and allied characters until every character has acted.  If all characters on one side have acted, the remaining characters take their turns in an order decided either by the GM or the players, as relevant.

When every character has taken a turn, the round ends and a new one begins.  The turn order continues to alternate, so if one side took the last turn in the previous round, the other side starts the new round.  This can result in hostile NPCs acting first in a new round.

## Types of Actions

Characters can choose between several different kinds of action, depending on what they want to achieve.  On their turn, characters can make a **standard move** and take either two **quick actions** or one **full action**.  The same action cannot be taken more than once per turn, except in certain cases.

In addition to move, quick, and full actions, there are several types of special actions: **free actions, reactions,** and **exertion**.  Unlike the standard action types, there is no limit on how many **free actions** and **reactions** a character can take per round.  Characters can even take reactions outside of their turn.

Miscellaneous activities like talking, wiping sweat out of someone’s eyes, slapping a button, and taunting an enemy aren’t considered actions and can be done any time during a character’s turn. If a player has a specific goal and outcome in mind, like taunting an enemy to draw their attention away from an injured friend, then it might be an action, but if it’s just a matter of talking smack, there’s always time.

### Standard Move

Movement up to a character's maximum speed.

### Quick Action

Actions that take a few moments, such as quickly firing a weapon, activating a system, or moving further.

### Full Action

Actions that require full attention (e.g. firing multiple weapons, performing field medicine).

### Free Action

Free actions are special actions granted by character traits, talents, or skills. Characters can take free actions at any point during their turn, and they don’t count toward the number of quick or full actions they take. They can also be used to take actions more than once per turn. For example, if a character can sprint as a free action, they can do so even if they have already used sprint in the same turn.

### Reaction

Reactions are special actions that can be made outside of the usual turn order as responses to incoming attacks, enemy movement, and other events.  Characters have two default reactions, each of which can be taken once per round; **Brace** and **Overwatch**.

### Exert

Characters can push themselves to the limit, allowing them to make an additional quick action.  Each time a character exerts, they gain a level of exhaustion.  Each point of exhaustion applies a disadvantage on any check performed by that character.  A character may take a full round action to recover and clear all of their exhaustion.

A character becomes unconscious when their exhaustion exceeds their character level + Con mod (minimum 1).  While unconscious, players lose one point of exhaustion per combat round and regain consciousness when their exhaustion reaches 0.

## Movement

On their turn, characters can always move spaces equal to their **speed**, in addition to any other actions. This is called a standard move to distinguish it from movement granted by systems or talents. 

A character only counts as moving if they move 1 or more spaces.

Characters can move into any adjacent space, even diagonally, as long as the space isn’t occupied by an obstruction.

### Obstruction

An obstruction is anything that blocks passage, preventing movement into its space entirely. Obstructions are typically environmental but other characters can also be obstructions.

Allied characters never cause obstruction, but characters still can’t end moves in their space.

### Engagement

If a character moves adjacent to a hostile character, they are both considered **engaged** for as long as they remain adjacent to one another.  Ranged attacks made by an **engaged** character or by an allied character targeting the enemy **engaged** character receive one difficulty to the attack.

### Involuntary Movement

When characters are pushed, pulled, or knocked in certain directions, it is called involuntary movement. Involuntary movement forces the affected character to move in a straight line, in a specified direction. When moving involuntarily, characters do not provoke reactions or engagement unless specified otherwise but are still blocked by obstructions.

### Difficult Terrain

**Difficult terrain** can be anything from rough, marshy, or swampy ground though to icy landscapes, and treacherous rocky scree.  All movement through difficult terrain is at **half speed** &mdash; each space of difficult terrain they move *into* is equivalent to two spaces of movement.

### Ranged Attacks

Make ranged attacks in order from top to bottom on the [ranged attack chart](#ranged-attack-chart).  Depending on the range of the attack, a weapon might be Advantaged (+), Disadvantaged (-), normal (' '), or unable to attack (X).

## Attacks and Wounds

To attack an enemy in combat, make a weapon skill check, adding any relevant bonuses to the total.  Use the [hit location chart](#wound-location) along with the lower die of the attack roll to determine where the attack hits.  Some enemies may not have a traditional, humanoid physiology, but the rules for resolving attacks are the same.

The target of the attack gains a Wound condition with a severity equal to the Impact of the throw.  Combined with the hit location, the target will get a condition like "Leg wound 1" or "Head wound 3".

If the total combined severity of a character's wounds exceeds the character's *max health*, the character goes out of action and cannot act until combat is over.  If a single body location takes damage exceeding the character's *max health*, they suffer an injury.  See the [injury chart](#injury-chart) for the effects.

PCs may avoid a wound by reducing their current Luck to mitigate the Impact of the attack.  They may not reduce their current Luck to less than 0.

Armor ignores damage from one attack of the wearer's choosing, then must be repaired or replaced.  Some weapon types (like lasers) can render conventional armor useless.

*example: Ryan is attacking Dustin and makes a melee attack.  He rolls a 6 and a 5, totaling 11.  Since the roll is greater than 8, the attack lands and the damage is equal to the Impact of the roll.  The damage is 4 (11 - 7) and hits Dustin's torso.  Dustin suffers a **Torso wound 4.***

## Combat Tables

### Ranged Attack Chart

The weapons detailed in the chart represent broad categories of firearms.

| Weapon | Short Range (30 ft) | Medium Range (100 ft) | Long Range (700 ft) | Distance Range (0.5+ miles) |
|---|---|---|---|---|
| Pistol | | - | X | X | 
| Scattergun | + | - | X | X |
| Carbine | | | - | X |
| Long Rifle | - | | | |

### Wound Location
| Lower Attack Die | Location |
|---|---|
| 1 | Leg |
| 2 | Arm |
| 3 - 5 | Torso |
| 6 | Head |

### Injury Chart
| Body Part | Effect |
|---|---|
| Head | Instant death. |
| Torso | Death in 1d6 hours without stablizing medical attention. | 
| Legs or Arms | A limb is destroyed.  Randomly determine left or right. |