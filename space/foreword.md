---
title: 'Foreward'
weight: -999
---

**By Trevor Fisher**

Welcome to the ***OpenSpace System Reference Document*** which outlines the rules for this OSR-inspired Open Gaming System.  OpenSpace aims to capture the spirit of improvisation and open-ended possibilities.  The system is designed to be sparse and adaptable to different sci-fi settings.
