---
title: 'Class List'
---

Classes in FIN are designed to be modular and mix/matched to create unique character archetypes.

## Class List (tentative)
* Assassin - critically harming enemies
* Animal Handler - animal companion, bonuses for animal interaction
  * Choose between agile, dense, or strong for companion
* Armor Master - increased bonuses from armor and shields
* Bard - performances, encouragement, buffs
* Beserker - use rage to deal more damage and to ignore damage
* Cleric - divine based magics
* Crafstman - pick a profession, have bonuses for crafting items from profession, can earn income
* Druid - nature based magics
* Enchanter - enhances spells for mind control
* Gunslinger - gunslinger talents, grit, shooting good
* Mage - arcane based magics
* Monk - martial class based around flurry of blows
* Paladin - divine fervor
* Ranger - benefits to tracking, scouting, and terrain navigation
* Rogue - stabby stabby, thievey thievey
* Warlock - occult based magics
* Weapon Master - pick a class of weapons, get bonuses