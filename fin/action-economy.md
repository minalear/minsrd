---
title: 'Action Economy'
---

The action economy in FIN is based off of Lancer, 5e, and Pathfinder 2e.  The main design goal is to keep actions simple and to keep combat flowing.

There are five types of actions in FIN; Full, Quick, Move, Reaction, and Free.  Each round, players may take a single move action, and then either two quick actions or a full action, as well as any number of free actions.  In addition, players may take a single reaction during the round at anytime, as long as the reaction's triggers are met.  Finally, players may perform an additional quick action on their turn, but may risk fatigue.

* **Full** - Full actions require a significant amount of time to perform.
* **Quick** - Quick actions take less time, allowing for more of them per round.
* **Move** - Move actions typically involve moving from point to point, but may also encompass other actions such as swapping items.
* **Reaction** - Reactions can be performed at anytime in response to another action, but only if the reaction's trigger conditions are met.
* **Free** - Very small actions that require little time or effort, such as stating a sentence or scoping the battlefield.

## Design Considerations
* How should we handle multiattack?
  * Could mimic the Lancer mechanic of quick actions cannot be the same
  * Quick action for a single attack, full round for multiattack
  * Level 1, why would someone full round attack if they can only attack once?
* Could consider using Pathinfer 2e's action economy instead, with everyone just having three actions
* In the end, it could be like Pathfinder where no one is using full round attacks until they have multiple attacks

## Actions
* Raising a shield takes a quick action and they usually grant a evasion bonus (+2)