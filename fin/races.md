---
title: 'Races'
---

Races are treated differently in FIN compared to other systems.  Stats/abilities are uncoupled from the player's race, leaving it as a backstory element.

## Traits
Traits give players bonuses and abilities based on their background, helping bring backstory elements to life.  Traits can be things like flat bonuses to abilities, skills, or unique abilities like spells or natural armor.