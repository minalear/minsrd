---
title: 'Skills'
---

## Strength
* Athletics - swim, climb, jump

## Dexterity
* Acrobatics
* Disable Device
* Sleight of Hand
* Stealth

## Intelligence
* Appraise
* Craft (*)
* Investigation
* Knowledge (Arcana)
* Knowledge (Engineering)
* Knowledge (Geography)
* Knowledge (History)
* Knowledge (Local)
* Knowledge (Nature)
* Knowledge (Nobility)
* Knowledge (Planes)
* Knowledge (Religion)
* Linguistics

## Wisdom
* Animal Handling
* Insight
* Medicine
* Survival

## Charisma
* Deception
* Disguise
* Intimidation
* Performance
* Persuasion


## Primary Skills

* Athletics (STR)
* Acrobatics (DEX)
* Sleight of Hand (DEX)
* Stealth (DEX)
* Investigation (INT/WIS)
* Animal Handling (WIS)
* Insight (WIS)
* Medicine (INT/WIS)
* Perception (WIS)
* Survival (INT/WIS)
* Deception (CHA)
* Intimidation (STR/CHA)
* Performance (CHA)
* Persuasion (CHA)

## Knowledge
* Arcana (INT)
* History (INT)
* Nature (INT)
* Religion (INT)
