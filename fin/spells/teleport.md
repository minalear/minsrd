---
title: 'Teleport'
---

* Can teleport yourself and up to one additional creature your size or smaller to a location
* Requires a Teleport check, the bonus being the familiarity of the target location
  * +0 zero familiarity
  * +5 been there and is familiar
* Partial success means you appear nearby with an error depending on the distance to the target location from where you initially cast the spell