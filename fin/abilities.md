---
title: 'Abilities'
---

Abilities in FIN are similar to the parent systems

* **Strength**
* **Dexterity**
* **Constitution**
* **Intelligence**
* **Wisdom**
* **Charisma**

Like 5e, each ability has an associated save