---
title: 'Magic Items'
---

Magical items are items created or enhanced with magical powers that produce additional effects, usually in flat bonuses or unique abilities.

## Enhancements
* The most common magic item are magic weapons and armor
* Enhancement bonuses vary from +1 to +3
  * Weapons receive an attack and damage bonus from their enhancement
  * Armor receives a DR bonus from their enhancement