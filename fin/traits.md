---
title: 'Traits'
---

Traits are a way for a player to customize their character by picking bonuses and abilities.

## Trait Ideas
* Hardy (base hit die increases one step)

## Detriment Ideas
* Fragile (base hit die decreases one step)

### Racial Traits

* Flight
* Darkvision
* Low-light vision
* Immune to magical sleep and paralysis effects
* Artificial (no eat, sleep, breathe)