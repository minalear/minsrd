---
title: "Priest"
---

Priests derive magical power from pledging service to a supernatural being. Service to such entities gives priests purpose in the world and causes for which they might fight. Faith in their patrons gives them the ability to work magic, which they perform through rite and prayer.
 
Discovering a tradition is a religious experience for priests. They encounter the presence of a god during their travels, feel some holy presence enter them while studying a religious text, or have a dream in which they are chosen to become divine servants. The initial experience sets these individuals on the priest’s path and gives them the power they need to further the interests of their immortal patrons.
 
Religion stands at the center of priests’ identities. It shapes their behavior, gives them purpose, and reveals their traditions. Priests committed to the New God use different kinds of magic from those who follow the teachings of the Old Faith. For more information on religions common to the Northern Reach, see Chapter 8.

### Level 1

- **Languages and Professions** You either read one language you can speak or add a language to the list of languages you can speak.  Also, add one religious profession.
- **Magic** You discover one tradition associated with your religion.  Then make two choices.  For each choice, either discover a tradition associated with your religion or learn one spell from a tradition you have discovered.
- **Shared Recovery** You can use an action to heal damage equal to your healing rate.  Then, choose one creature other than you that is within short range.  The target also heals damage equal to your healing rate.  Once you use this talent, you cannot use it again until after you complete a rest.

### Level 2

- **Magic** Make two choices. For each choice, you either
discover a new tradition associated with your religion or learn a spell from a
tradition you have discovered.
- **Prayer** When a creature within short range of you makes an attack roll or skill check, you can use a reaction to grant +1 on the roll.

### Level 5

- **Magic** Learn one spell from your traditions.
- **Divine Strike** When you use Prayer to grant a creature a bonus on their attack roll, the creature’s attacks with weapons deal 1d6 extra damage.

### Level 8

- **Magic** YLearn one spell from your traditions.
- **Improved Shared Recovery** You can use Shared Recovery twice per rest.