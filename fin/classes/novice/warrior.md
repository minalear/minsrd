---
title: "Warrior"
---

Extensive training with weapons and studying numerous fighting styles teach warriors how to fight and survive on the battlefield. Their skills depend on being physically fit, quick and nimble, or a combination of both. Upon completing their training, warriors can pick up and fight with almost any weapon, striking with greater precision and greater force than anyone else.

Although all warriors know how to fight, they distinguish themselves by the weapons they wield. Some favor archery and put their focus on ranged weapons. Others fight with swords and axes, using their might to overcome their foes. Others still favor swift weapons, slipping rapier or saber strikes through their enemies’ defenses.

Warriors come from all backgrounds. They are howling barbarians tumbling out from the depth of the wilderness, veteran soldiers marching in the Empire’s armies, hard-bitten mercenaries, mystics who transform their bodies into weapons, or anyone else who knows how to win battles through superior skill at arms.

### Level 1

- **Languages and Professions** You add one common, martial, or wilderness profession.
- **Catch Your Breath** You can use an action or a triggered action on your turn to heal damage equal to your healing rate. Once you use this talent, you cannot use it again until after you complete a rest.
- **Weapon Training** When attacking with a weapon, you make the attack roll with 1 boon.

### Level 2

- **Combat Prowess** Your attacks with weapons deal 1d6 extra damage.
- **Forceful Strike** When the total of your attack roll is 20 or higher and exceeds the target number by at least 5, the attack deals 1d6 extra damage.

### Level 5

- **Combat Expertise** When you use an action to attack with a weapon, you either deal 1d6 extra damage with that attack or make another attack against a different target at any point before the end of your turn.

### Level 8

- **Grit** You can use Catch Your Breath twice between each rest.
- **Combat Mastery** When you use an action to attack with a weapon, you either deal 1d6 extra damage with that attack or make another attack against a different target at any point before the end of your turn. This talent is cumulative with Combat Expertise. You must choose a different target for each attack you make.