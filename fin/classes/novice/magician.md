---
title: "Magician"
---

Magicians strive to reach the heights of magical power. If they follow this journey to its end, choosing paths to complement what they have learned, they join the most powerful users of magic in the world.

Hopeful magicians must first discover a tradition of magic to begin learning spells. Discovery can be accidental, resulting from being affected by a spell, stumbling into an area steeped in magical energy, or finding power within oneself. Discovery can also be taught. Ancient institutions of magic, wizards, witches, and others reveal traditions to promising students.

Upon discovering a tradition, the magician learns the most basic spells from it. All this prepares magicians for the process of learning greater and more powerful spells.

Since magicians freely choose their traditions, they display a range of capabilities. Some favor destructive magic, learning spells that let them harness elemental forces of wind, rain, fire, and earth. Others prefer subtler magic, favoring charms to manipulate the minds of others or illusions to deceive and conceal. Magicians can also be conjurers, loosing monsters to fight on their behalf, or engineers, creating servants and machines from spare parts they pick up along the way.

The possibilities magic offers also present many perils. More than one magician has succumbed to dark magic’s temptations, dabbling into the arts of Forbidden, Necromancy, or worse traditions. Such magic almost always corrupts the magician, but those seeking its power rarely care.

## Starting Statistics


### Level 1

- **Languages and Professions** You read all the languages you know how to speak.  In addition, you add one academic area of knowledge of your choice.
- **Magic** You discover one tradition.  Then, make three choices.  For each choice, you either discover another tradition or learn a spell from a tradition you have discovered.
- **Cantrip** Whenever you discover a tradition, you learn an extra rank 0 spell from that tradition.
- **Sense Magic** You learn the *[sense magic](#)* spell.

### Level 2

- **Magic** Make two choices. For each choice, you either
discover a new tradition or learn a spell from a
tradition you have discovered.
- **Spell Recovery** You can use an action to heal damage
equal to your healing rate and regain one casting
you expended of a spell you learned. Once you use
this talent, you cannot use it again until after you
complete a rest.

### Level 5

- **Magic** You discover a tradition or learn
one spell.
- **Counterspell** When a creature you can
see attacks you with a spell,
you can use a triggered action to
counter it. The triggering creature
makes the attack roll with 1 bane and
you make the challenge roll to resist
it with 1 boon.

### Level 8

- **Magic** You discover a tradition or learn
one spell.
- **Improved Spell Recovery** When you use Spell Recovery, you
regain two castings instead of one.