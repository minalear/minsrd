---
title: 'Class List'
---

## 5e Classes 

* Barbarian
* Bard (CHA)
* Cleric (WIS)
* Druid (WIS)
* Fighter
* Monk
* Paladin (WIS or CHA)
* Ranger
* Rogue
* Sorcerer (CHA)
* Warlock (INT or CHA)
* Wizard (INT)

## Novice Paths

* [Magician](/grand/appendix/classes/novice/magician.md)
* [Priest](/grand/appendix/classes/novice/priest.md)
* [Rogue](/grand/appendix/classes/novice/rogue.md)
* [Warrior](/grand/appendix/classes/novice/warrior.md)

## Expert Paths

### Paths of Faith

* [Cleric](/grand/appendix/classes/expert/cleric.md)
* [Druid](/grand/appendix/classes/expert/druid.md)
* [Oracle](/grand/appendix/classes/expert/oracle.md)
* [Paladin](/grand/appendix/classes/expert/paladin.md)

### Paths of Power

* [Artificer](/grand/appendix/classes/expert/artificer.md)
* [Sorcerer](/grand/appendix/classes/expert/sorcerer.md)
* [Witch](/grand/appendix/classes/expert/witch.md)
* [Wizard](/grand/appendix/classes/expert/wizard.md)

### Paths of Trickery

* [Assassin](/grand/appendix/classes/expert/assassin.md)
* [Scout](/grand/appendix/classes/expert/scout.md)
* [Thief](/grand/appendix/classes/expert/thief.md)
* [Warlock](/grand/appendix/classes/expert/warlock.md)

### Paths of War

* [Berserker](/grand/appendix/classes/expert/berserker.md)
* [Fighter](/grand/appendix/classes/expert/fighter.md)
* [Ranger](/grand/appendix/classes/expert/ranger.md)
* [Spellbinder](/grand/appendix/classes/expert/spellbinder.md)

## Master Paths

### Paths of Magic

* [Abjurer](/grand/appendix/classes/master/abjurer.md)
* [Aeromancer](/grand/appendix/classes/master/aeromancer.md)
* [Apocalyptist](/grand/appendix/classes/master/apocalyptist.md)
* [Arcanist](/grand/appendix/classes/master/arcanist.md)
* [Astromancer](/grand/appendix/classes/master/astromancer.md)
* [Bard](/grand/appendix/classes/master/bard.md)
* [Beastmaster](/grand/appendix/classes/master/beastmaster.md)
* [Chronomancer](/grand/appendix/classes/master/magician.md)
* [Conjurer](/grand/appendix/classes/master/magician.md)
* [Destroyer](/grand/appendix/classes/master/magician.md)
* [Diviner](/grand/appendix/classes/master/magician.md)
* [Enchanter](/grand/appendix/classes/master/magician.md)
* [Geomancer](/grand/appendix/classes/master/magician.md)
* [Healer](/grand/appendix/classes/master/magician.md)
* [Hexer](/grand/appendix/classes/master/magician.md)
* [Hydromancer](/grand/appendix/classes/master/magician.md)
* [Illusionist](/grand/appendix/classes/master/magician.md)
* [Mage Knight](/grand/appendix/classes/master/magician.md)
* [Magus](/grand/appendix/classes/master/magician.md)
* [Necromancer](/grand/appendix/classes/master/magician.md)
* [Pyromancer](/grand/appendix/classes/master/magician.md)
* [Runesmith](/grand/appendix/classes/master/magician.md)
* [Savant](/grand/appendix/classes/master/magician.md)
* [Shapeshifter](/grand/appendix/classes/master/magician.md)
* [Stormbringer](/grand/appendix/classes/master/magician.md)
* [Technomancer](/grand/appendix/classes/master/magician.md)
* [Tenebrist](/grand/appendix/classes/master/magician.md)
* [Thaumaturge](/grand/appendix/classes/master/magician.md)
* [Theurge](/grand/appendix/classes/master/magician.md)
* [Transmuter](/grand/appendix/classes/master/magician.md)
* [Traveler](/grand/appendix/classes/master/magician.md)
* [Woodwose](/grand/appendix/classes/master/magician.md)

### Paths of Skill

* [Acrobat](/grand/appendix/classes/master/magician.md)
* [Avenger](/grand/appendix/classes/master/magician.md)
* [Blade](/grand/appendix/classes/master/magician.md)
* [Brute](/grand/appendix/classes/master/magician.md)
* [Cavalier](/grand/appendix/classes/master/magician.md)
* [Champion](/grand/appendix/classes/master/magician.md)
* [Chaplain](/grand/appendix/classes/master/magician.md)
* [Conqueror](/grand/appendix/classes/master/magician.md)
* [Death Dealer](/grand/appendix/classes/master/magician.md)
* [Defender](/grand/appendix/classes/master/magician.md)
* [Dervish](/grand/appendix/classes/master/magician.md)
* [Diplomat](/grand/appendix/classes/master/magician.md)
* [Dreadnaught](/grand/appendix/classes/master/magician.md)
* [Duelist](/grand/appendix/classes/master/magician.md)
* [Engineer](/grand/appendix/classes/master/magician.md)
* [Executioneer](/grand/appendix/classes/master/magician.md)
* [Exorcist](/grand/appendix/classes/master/magician.md)
* [Explorer](/grand/appendix/classes/master/magician.md)
* [Gladiator](/grand/appendix/classes/master/magician.md)
* [Gunslinger](/grand/appendix/classes/master/magician.md)
* [Infiltrator](/grand/appendix/classes/master/magician.md)
* [Inquisitor](/grand/appendix/classes/master/magician.md)
* [Jack-of-All-Trades](/grand/appendix/classes/master/magician.md)
* [Marauder](/grand/appendix/classes/master/magician.md)
* [Miracle Worker](/grand/appendix/classes/master/magician.md)
* [Myrmidon](/grand/appendix/classes/master/magician.md)
* [Poisoner](/grand/appendix/classes/master/magician.md)
* [Sentinel](/grand/appendix/classes/master/magician.md)
* [Sharpshooter](/grand/appendix/classes/master/magician.md)
* [Templar](/grand/appendix/classes/master/magician.md)
* [Weapon](/grand/appendix/classes/master/magician.md)
* [Zealot](/grand/appendix/classes/master/magician.md)