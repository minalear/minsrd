---
title: 'Weapon Master'
---

## Tier 1
* Select a class of weapon
* You gain proficiency with these weapons
* You may select this tier again for each level placed into weapon master.

## Tier 2
* Select a class of weapon you are proficient in
* You gain +1 to hit/damage with these weapons