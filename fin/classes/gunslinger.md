---
title: 'Gunslinger'
---

## Tier 1
* You gain proficiency with firearms.
* If you are already proficient with firearms, instead you gain +1 to hit/damage with firearm attacks.

## Tier 2
* You gain two Gunslinger Talents.