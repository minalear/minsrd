---
title: 'Armor'
---

Armor levels and qualities can vary between materials and enhancements

## Basic Armor
| name     | type   | dr | other |
|----------|--------|----|-------|
| Padded   | light  | 1  | |
| Leather  | light  | 2  | |
| Studded  | medium | 3  | |
| Chain    | medium | 4  | |
| Banded   | heavy  | 5  | |
| Plate    | heavy  | 6  | |