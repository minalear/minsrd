---
title: 'Weapons'
---

## Basic Weapons
| name   | type | dmg | other          |
|--------|------|-----|----------------|
| Dagger | S,P  | 1d4 | concealing     |
| Rapier | P    | 1d6 | Armor Piercing |

## Weapons Table

### Melee Weapons

| Name       | Damage |
|------------|--------|
| Dagger     | 1d4    |
| Shortsword | 1d6    |
| Longsword  | 1d8    |
| Glaive     | 1d10   |
| Great Axe  | 1d12   |

### Ranged Weapons

| Name | Damage |
|------|--------|
| Blowgun | 1d2 | 
| Sling | 1d4 |
| Shortbow | 1d6 |
| Longbow | 1d8 |
| Crossbow (L) | 1d8 |
| Crossbow (H) | 1d10 |