---
title: 'Defense'
---

The most complicated part of FIN is how defense works against physical and magical attacks.

* **Evasion** - rating on how well someone can avoid being hit
* **Armor** - rating that reduces the amount of physical damage taken
* **Magic Defense** - rating that boosts the magical resilience

## Design Considerations
* Having high DR can make balancing low level enemies damage difficult
  * Maybe flanking can ignore DR?
  * Heavier armor could reduce movement speed/evasion
  * Having a small cap on DR can make armor pointless at higher levels
* Pathfinder has an alternate rule system for treating armor as DR
  * Creatures larger than you can overcome DR by just being big
  * Adamantine is the material that they use to bypass armor