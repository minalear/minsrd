---
title: "Design Notes"
---

## Checks

* Checks are made with dice pools
* Successes are typically are on a 4+, but this may change
* Inspired checks allow for "exploding sixes" where a player may add an additional die to the pool for every six that is rolled
* A character's proficiency level indicates the amount of dice to roll for that check
* The number of successes required to pass a check is referred to as the difficulty check, or DC.
* Sources of advantage add dice to the pool
  * Players should argue for sources of advantage, disregarding them if they do not wish to use them
  * This is due to how advancement works, where players are encouraged to perform challenging checks to advance their skills
* Sources of disadvantage increase the difficulty
* Advantage and disadvantage do not cancel each other out
* Standard Checks - DM sets the DC and the player rolls to meet the DC.  Pass/Fail
* Versus Checks - Both characters roll their relevant check; the one with the most successes "wins"
* Graduated Checks - used for checks that take time, like research.
  * DM sets a goal and the players rolls checks, counting up successes.
  * Can take several rolls to meet the goal.
  * DM determines when and how checks are allowed to be made.
* Linked Checks - series of checks, using different skills, in sequence to represent a complex task.
  * The pervious check affects the next task; meeting the DC provides no benefit or penalty.
  * Exceeding the DC grants an advantage on the next check.
  * Failing imparts a disadvantage on the next check.
* Checks should only be rolled for when there is some kind of pressure (like time) and there is a chance for failure.
  * Any task that is plausible and the player has plenty of time should require no check.
  * Players should have an idea what the consequence for failing a check is before rolling.
* Checks can be made carefully, patiently, or quickly
  * Carefully increases the time required by half and grants an advantage
    * Must be declared before the roll is made
  * Patiently - extra successes can be used by the player to embellish the final product, mostly used as a narrative device
    * Player is crafting a sword and uses the extra successes to describe the nice hilt
    * Can be declared after the roll is made
  * Quickly - each success past the DC reduces the time taken by 10%
* Checks cannot be made again unless conditions legitimately and drastically change regarding the check
* Help - provides additional die to the check
  * The player that is helping provides one of their dice to the other player to determine if they actually helped or not (a success counts as successfully helping)
  * The helper must be in the scene and describe how they're helping
  * A more skilled the helper is, the more die they provide to the check; proficiency 4 or less is 1 die, 5 or more is 2 dice
* Fields of Related Knowledge (FoRK)
  * A character's related skills can help their other skill checks, as long as they're related, and grants an advantage on the check
    * If the skill's proficiency is 7 or higher, then +2D
  * Forks are situational and requires the player to suggest them.  The DM is the final arbiter on what can or cannot be applied.
  * Players are required to describe how their Forks affect their actions.
  * Only skills can be used as Forks.  Stats and attributes may never FORK.
* Players may not make a check if their proficiency is 0
* Beginner's Luck - if a player does not have a skill, they may test with a stat instead at double the DC
  * Only the base DC is doubled, disadvantages are added after the fact
* Checks made without the appropriate tools are made at double the DC (which can in addition be doubled by the beginner's luck penalty)
  * When using an expendable tool, roll a Fate Die to determine if their use is expended (1 it is expended and needs to be replenished)

## Stats

* Will - social stat representing a character's strength of mind, empathy, and social intelligence.  It is the basis of a number of skills and attributes.
* Perception - awareness and mental acuity stat
* Dexterity - represents the hand-eye coordination of a character
* Speed - how fast the character moves and how coordinated they are overall.
* Power - physical strength
* Forte - toughness and durability.  Makes up health and mortal wounds

## Attributes

* Health - used in game to recover from injury and resist the effects of pain and fatigue
* Reflexes - used to determine how often a character acts during a fight
* Steel - how steady a character's nerves are
* EMOTIONS - optional attribute some characters have, like Faith, Grief, Greed, and Hatred
* Mortal Wound - point at which the character dies due to injuries

## Advancement

* Always use the total dice rolled rather than the proficiency level to determine what is considered routine, difficult, or challenging for advancement
  * Helping, Forking, and seeking advantages makes tests easier and the characters learn less as a result

