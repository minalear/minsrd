---
title: 'GrandRPG'
---

GrandRPG is a basic roleplaying game taking inspiration from other game systems and adapting them into a simplified ruleset for generic roleplaying games.