---
title: 'Combat'
---

When a fight is simple, resolve it with a single check.  If the outcome is a foregone conclusion, just say what happens.  For more complex skirmishes, use these rules.

## Attacks and Defense

* Roll an attack
* If the attack hits, then the defender can make a reaction to potentially negate the hit
* If the attack is not negated, the attacker rolls damage and any effects are applied to the defender

* Defense reactions are a way to get around scaling difficulties when hitting different types of targets
  * I wanted to keep the "fail/success/partial success" aspect of making rolls but didn't want a scaling AC or extra math for things like heavily targeted foes or nimble adversaries
  * Everyone could have access to basic reactions like DODGE or BRACE
  * Class abilities, equipment, or other things can grant additional reactions like BLOCK, PARRY, or REFLECT
  * Some defense reactions require a specific reaction action, so only one per turn can be performed, but some probably should be accessible whenever (like armor)
    * DODGE can be an improved version of the basic avoidance while BRACE can be an improved version of the basic armor
* Magic attacks can follow a similar formula too where most magic attacks just require a RESIST check
  * Some magic attacks could be avoidable or even blockable
* Glancing blows happen when either the attack or the defense is a partial success
  * In these cases, if just one of them is partial and the other is failure, then the damage is halved
  * If both are partial successes, then the attack fails to connect

* Armor is categorized in the basic RESIST check but some can offer flat damage reduction in addition to their defense bonus