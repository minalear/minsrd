---
title: "Core Mechanics"
---

## Dice

Burning Wheel uses common six-sided dice for most rolls.  Dice are rolled in handfuls (or pools) of usually three to six dice.  In each roll, every die is counted as either as a **Success** or a **Failure**.  Most rolls have a of 4+ difficulty where 4, 5, or 6 count as a success, whereas 3, 2, or 1 will count as a failure, though there are scenarios where this can change.

### Fate Die

Sometimes, a Fate Die is required to be rolled.  For these, just roll a single D6 and note the result.

## Stats

There are six stats that describe a character's basic mental and physical composition; Will, Perception, Agility, Speed, Power, and Forte.

### Will 

Will is the social stat.  It represents the character's strength of mind, empathy, and social intelligence.  It is the basis of a number of skills and attributes.

### Perception

Perception is the awareness and mental acuity stat.  It's rolled all the time in game.  Players always have their characters searching for information or clues, and this is the stat rolled in those situations.  Perception is the root of many skills in the game.

### Agility

Agility represents the hand-eye coordination of the character.  It is rolled when a character must keep hold of something, snatch something, or toss an object.  It is also the basis for many martial skills.

### Speed

This quirky stat represents how fast the character moves and how coordinated they are overall.  It's rolled all the time in game, from fleeing your enemies to ducking a blow.  It is also the basis for the Dancing, Stealthy, and Climbing skills.

### Power

Power represents physical strength.  It's rolled in game when the character has to push something over or grapple with someone.  It is also the basis for damage in melee attacks.

### Forte

Forte represents the character's physical mettle&mdash;how tough and durable they are.  Forte is rarely rolled in game, but it is a part of two crucial attributes; Health and Mortal Wound.

## Attributes

Attributes are derived from stats and are used for more subtle checks than the raw instances described above.  They can also be avenues to additional abilities, available only to certain characters.

## Attributes

### Health

The Health attribute is derived from Will and Forte.  It is used in game to recover from injury and resist the effects of pain and fatigue.

### Reflexes

The Reflexes attribute is calculated from Perception, Agility, and Speed.  It is used to determine how often a character acts during a fight.  Reflexes isn't rolled like other abilities.

### Steel

Steel is a complex attribute that combines elements from different stats and the character's background to determine how steady a character's nerves are.  Steel has its own section and is discussed in detail there.

### Mortal Wound

Mortal Wound combines Power and Forte to set the point at which the character dies in the injury mechanics.  Mortal Wound is never rolled; it is just a marker.

### Emotional Attributes

Some characters possess an additional ability tied into their emotional makeup; Faith, Grief, Greed, and Hatred are a few examples.  These attributes are factored by answering questions about the character and are tested by special circumstances in game.  Emotional Attributes have their own chapter and are further detailed in the individual lifepath chapters.

## Skills

A skill is a specialized field of knowledge or particular technique known by the character.  A skill allows a character to achieve their goals more easily than by using stats.  Example of skills include: Animal Husbandry, Mending, Sorcery, Sword, and Ugly Truth.

## Proficiency

The number next to your character's stats, attributes, and skills indicates how many dice you roll and is referred to as your character's **Proficiency**.  Proficiency represents your character's actual ability and competency in that area.  

| Level      | Description |
|------------|-------------|
| Prof 1     | naturally disinclined or utterly incompetent |
| Prof 2     | untrained, raw, weak, or unpracticed |
| Prof 3     | nominally trained and practiced |
| Prof 4     | competent; everyday stuff doesn't pose a challenge |
| Prof 5     | expert |
| Prof 6     | near mastery |
| Prof 7     | excellence |
| Prof 8     | total mastery, complete understanding |
| Prof 9     | uncanny; incomprehensibly good |
| Prof 10    | as near perfection as the system allows |

## Checks

Checks are made anytime a character performs an action where the outcome is undetermined.  The difficulty of the check is referred to as the **Difficulty Class** or **DC**.  Whenever a check is made, the DM will outline the possible outcomes for that check.

Checks cannot normally be retried after failure.  To retry a check, the conditions surrounding the check must legitimately and drastically change.

### Standard Checks

Standard checks are straightforward.  Players roll their proficiency, count up the number of successes and compares it against the DC.  If the number of successes equals or exceeds the DC, then the check passes.

### Versus Checks

When two characters come into oppositions, they make a versus check.  Both players roll their relevant ability and compare their number of successors.  The one with the most successes is considered the victor.  If there is an aggressor, then ties go to the defender.  If both characters are considered the aggressor, then ties result in a deadlock and no edge is gained in the conflict.

### Graduated Checks

Some checks have no fixed difficulty.  It is simply a matter of checking an ability with the results being determined by the number of successes.  This is often used for checks like research, searching an area, or other knowledge-based skills.  The amount of information distributed by the DM is directly dependent on the number of successes rolled.

The information available in graduated checks is rated and judged along the standard difficulty ratings; One is considered obvious stuff, five being expert knowledge, and ten being miraculous understanding.

### Linked Checks

Linked Checks are used to determine long term or complex operations that require many skill or ability checks to overcome.  Linked checks can be performed by a single character with many skills or multiple characters.  Tasks happen sequentially during a linked check, not all at once.

Whenever a check is made, if the character matches the DC, then there is no benefit to the next check.  If the check exceeds the DC, then the next check gains an advantage.  If the check fails, then the next check gains a disadvantage.

## Advantage and Disadvantage

Conditions surrounding a check can modify how the check is made.  For each source of **Advantage**, add +1D to the roll.  For each source of **Disadvantage**, increase the DC by +1.  Advantages and disadvantages do not cancel each other out.

### Advantage 

Players are the ones that lobby for advantages to the check and only when the source of the advantage is clear and concise.  A player can only lobby for one +1D advantage per check.  In order to gain the advantage, they must state how and why they deserve such a boon&mdash;no situation lawyering.

In the case of social skill checks, good roleplay, keen description, or just good timing are enough to earn an advantage die.

## Double Difficulty

Some rolls are penalized with the double difficulty penalty, effectively doubling the base DC of a check (disadvantage to a check isn't doubled).  The penalty is rare but can occur for untrained skill checks (see [Beginner's Luck](#unskilled-checks-beginners-luck)) or when a character lacks the proper tools for the check.

## Check Methods (Carefully, Patiently, Quickly)

A check can be made normally or it can be made *carefully, patiently, or quickly*.  These three can have a different effect on the roll or difficulty.

### Carefully

Working carefully increases the time for a check by half, but grants a +1D advantage.  If a character wishes to work carefully, they must state this before the roll.  Failing a careful check typically means that the character ran out of time and suffers the consequences of the failure.

### Patiently

Working patiently allows a player to allocate extra successes to the quality of the finished product, typically to embellish a description or to add a flourish.  They are mostly a narrative device but some skills have their own rules for allocation extra successes and their effects.  

Extra successes may be allocated to working patiently after the dice are rolled.

### Quickly

Working quickly allows a character to complete a task in a shorter amount of time.  Like working patiently, each success past the DC can be allocated to reducing the time the check took by 10%.

### Mixing Method

Players may have their characters work carefully, patiently, *and* quickly all at once.  Each extra success die can be allocated between patiently or quickly.

## Help

Players may have their characters help one another in the game.  When two more more characters are acting together, only one player rolls.  They accept much of the risk but shares the reward.

When helping, the player that is helping gives the other player one of their dice, which is used to determine if that character succeeded in helping or not.  The helper must be in the scene and describe how they're helping.

The more skilled the helper is, the more dice they provide to the check.  For proficiency 1-4, they grant one die.  For proficiency 5+, they provide two dice.

## Fields of Related Knowledge

Some checks can benefit from the character having a wide array of skills.  A player may have their character use their varied experience to help with the check.  This is called using your Fields of Related Knowledge or **Forks**.

When making a check, each related skill the character possesses can be used like help to add advantage to the check; +1D if the related skill is proficiency 1-7 or +2D if the proficiency is 7+.

Forks are situational.  The players suggest them through roleplay and the DM arbitrates which are applicable and which are not.  Players describe how their Forks affects their actions.

Only skills can Fork; stats and attributes may never Fork.

## Unskilled Checks (Beginner's Luck)

If a character does not have an appropriate skill for a check, they may check using a relevant stat instead.  However, whenever a stat is rolled in place of a skill, the DC is [doubled](#double-difficulty).

Note: The first Sorcerous check a character makes must be made through some Instruction and cannot be made through Beginner's Luck or Practice.  However, after the first check, then Beginner's Luck can be used on further checks.

## Tools

Some skills are listed with a requirement for tools.  Checks made without the necessary tools suffer a [double difficulty penalty](#double-difficulty).  Note: this can be combined with the penalty from Beginner's Luck, effectively quadrupling the DC of a check.

When using expendable tools, roll a Fate Die each time they are used after the first.  A result of 1 indicates the tools have been used up and must be replenished or repaired by the appropriate means.

## Beliefs

A player must determine the three top priorities for their character.  These are fundamental ideas important to the character.  They are a combination of the outlook of the character and the goals of the player.

These are not general beliefs, like "God" or "Country."  They are explicitly stated drives that tie directly into the world and setting.  Examples of Beliefs are "I must serve the Ecclesiarch so that I might be redeemed for my crime," or "I will protect my friend's sister at any cost."

When sculpting your character's Beliefs, thank; WHat do I want out of this character and this situation?  How can my character's Beliefs reflect that?  Then shape your character's Beliefs to reflect those priorities.

Beliefs are not arbitrarily chosen.  You relate each one to what is going on in the game.  They bind your character into the world.  As they are challenged, they give you the chance to express what your character is about.

Beliefs are meant to be challenged, betrayed, and broken.  Such emotional drama makes for a good game.  If your character has a Belief, "I guard the prince's life with my own," and the prince is slain before your eyes in the climax of the scenario, that's your chance to play out a tortured and dramatic scene.

### Changing Beliefs

A player may change their character's Beliefs as they see fit.  Characters are meant to grow and change through play.  Changing Beliefs is a vital part of that growth.  However, the DM has final say over when a Belief may be changed.  If they feel the player is changing a Belief to wriggle out of a difficult situation and not as part of character growth, then they may delay the change until a time they see as appropriate.

## Instincts

Instincts are similar to Beliefs&mdash;they are player-determined priorities and reactions for the character.  They are based on the character's experiences and have a tangible function.

An Instinct is essentially an "if/then" or "always/never" statement for the character's behavior.  "If surprised, I draw my sword." is a common instinct.  The player is allowed to program these actions and reactions into their character.  Therefore, they can be assured that their character will react within certain parameters whether the player explicitly states it or not at that moment in play.  Think of them as hardwired reactions from training and experience.

Instincts are a mechanical way to ensure that your character behaves in a certain manner which can't be contravened by the DM.

### Changing Instincts 

A player may change their character's Instincts as they see fit&mdash;as the character's experience in play changes them.  However, the DM has final say over when an Instinct may be changed.  If they feel the player is changing an Instinct not as part of character growth, then they may delay the change until a time that they see as appropriate.

## Traits 

Traits are quirks and odd abilities that the character acquires through the course of their life.  A player stats by purchasing traits in character creation, but they will also be earned in play.

Traits affect a character's personality, their appearance, and even grant them special abilities.  Where Beliefs and Instincts are relatively loosely defined, the role of traits in game is mechanically fixed.

There are three kinds of traits: Character, call-on, and die traits.

### Character Traits

A character trait illustrates a prominent aspect of a character's psychological or physiological make-up&mdash;something that affects how the character will be roleplayed at the table, something that says, "You're not just someone, you're *it.*"  Anyone can say their character is hairy, but unless they pay the trait point, it's hairy with a lower-case "h".  Pay the point and then they're the hairiest person around.

By choosing these traits, the player is stating that they are going to do one of two things; either they're going to play those traits and exemplify them or they're saying that their character is starting with these traits, but they're about to change.

In the second case, character traits are used as the crux for creating all sorts of problems for their character in game.  They're going to use their traits to get their character into situations where they have to make hard decisions.  Do I go with mny nature or do I fight against it?

### Call-On Traits

This type of trait is powerful and subtle.  When its conditions are met, traditional game mechanics are thrown out.  They typically are used to break a tie or to allow the player to reroll a failed check.

Players decide when and how these traits are invoked.  Call-ons may only be invoked once per session.  They are a powerful aid, representing an unpredictable edge the character possesses.

### Die Traits

Die traits modify an ability in the same way every time they are used.  Either they add a die or two, change a mechanical number&mdash;a DC or hesitation&mdash;or grant a new ability/new way to roll dice.

## Changing Traits and Earning New Ones

At the end of a campaign or extended narrative arc, the DM and the players nominate characters to receive new traits (or remove current ones).  Going around the table, players discuss each character who participated in the adventure.  Possible traits are suggested for all the characters.  After all characters have been discussed, the players vote on teh traits suggested for each character.  The owning player doesn't vote for his own traits.  A unanimous decision grants the trait.  Character, call-on, and die traits can be awarded.

Characters can be awarded traits that change their bodies&mdash;scars, a limp, or even missing digits&mdash;to represent their in-game experiences.  They can (and should) be awarded character traits according to how the other players perceive their personalities in the course of play.  This shouldn't be a punishment but an honest outward reflection of what's been happening at the table.

## Evolving Beliefs and Instincts

Beliefs and Instincts are meant to evolve into traits throughout the course of play.  Spending Artha on a Belief or Instinct is a good benchmark for the type or power level of a trait.  The more Artha spent, the more potent the trait.

## Resources and Circles

### Resources*

Whether wealthy or destitute, every character is assigned a Resources ability.  Its starting proficiency is determined by how the player spent their character's resource points in character creation.  Thereafter, it is used as a skill in game.  Its function is to determine how economically and financially solvent a character is: Can they afford to buy property?  Can they keep their equipment in repair?  Are they forced to beg for food and lodging?  The Resources ability allows all of this to be determined by a roll of the dice.

### Circles

Who does the character know from their days as an apprentice?  Can they call on their former gang mates for help?  Such questions are answered using the Circles ability.  It's a measure of the character's social influence, and its scope is shaped by the character's lifepaths.