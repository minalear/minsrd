---
title: "Advancement"
---

A character's abilities are advanced by making checks.  As characters make checks, players mark the ability's experience log.  Mark enough checks of varying difficulty and the ability advances.

## Routine, Difficult, and Challenging

Each check falls into one of three levels of difficulty for advancement; routine, difficult, and challenging.  The difficulty of a check is found by comparing the number of dice rolled against the DC.

Note: Dice from Forking or Help are counted as "dice rolled" when it comes to advancement.  Forked skills do not advance, only the primary ability does.  Extra dice from Artha **never** counts towards this number.

| Dice Rolled | Routine | Difficult | Challenging |
|-------------|---------|-----------|-------------|
|  1D | DC 1*   | DC 1*    | DC 2+  |
|  2D | DC 1    | DC 2     | DC 3+  |
|  3D | DC 1-2  | DC 3     | DC 4+  |
|  4D | DC 1-2  | DC 3-4   | DC 5+  |
|  5D | DC 1-3  | DC 4-5   | DC 6+  |
|  6D | DC 1-4  | DC 5-6   | DC 7+  |
|  7D | DC 1-4  | DC 5-7   | DC 8+  |
|  8D | DC 1-5  | DC 6-8   | DC 9+  |
|  9D | DC 1-6  | DC 7-9   | DC 10+ |
| 10D | DC 1-7  | DC 8-10  | DC 11+ |
| 11D | DC 1-8  | DC 9-11  | DC 12+ |
| 12D | DC 1-9  | DC 10-12 | DC 13+ |
| 13D | DC 1-10 | DC 11-13 | DC 14+ |
| 14D | DC 1-11 | DC 12-14 | DC 15+ |
| 15D | DC 1-12 | DC 13-15 | DC 16+ |
| 16D | DC 1-13 | DC 14-16 | DC 17+ |
| 17D | DC 1-14 | DC 15-17 | DC 18+ |
| 18D | DC 1-15 | DC 16-18 | DC 19+ |

## Number of Checks Required for Advancement

To advance an ability exponent one rank, a player must accumulate a certain number of routine, difficult, and challenging tests.

### Advancing Skills

| Proficiency | Routine | | Difficult | | Challenging |
|-------------|---------|-|-----------|-|-------------|
| 1 | 1       | and | 1 | or | 1 |
| 2 | 2       | and | 1 | or | 1 |
| 3 | 3       | and | 2 | or | 1 |
| 4 | 4       | and | 2 | or | 1 |
| 5 | &mdash; |     | 3 | or | 1 |
| 6 | &mdash; |     | 3 | or | 2 |
| 7 | &mdash; |     | 4 | or | 2 |
| 8 | &mdash; |     | 4 | or | 3 |
| 9 | &mdash; |     | 5 | or | 3 |

### Advancing Stats

| Proficiency | Routine | | Difficult | | Challenging |
|-------------|---------|-|-----------|-|-------------|
| 1 | &mdash; | | 1 | and | 1 |
| 2 | &mdash; | | 1 | and | 1 |
| 3 | &mdash; | | 2 | and | 1 |
| 4 | &mdash; | | 2 | and | 1 |
| 5 | &mdash; | | 3 | and | 1 |
| 6 | &mdash; | | 3 | and | 2 |
| 7 | &mdash; | | 4 | and | 2 |
| 8 | &mdash; | | 4 | and | 3 |
| 9 | &mdash; | | 5 | and | 3 |

### Advancing Attributes

Reflexes and moral wounds advance as their parent stats increase the applicable average.  Health and Steel advance as skills and are not dependent on their parent stats after character creation.  Faith, Grief, and Hate all advance as skills as well.  Greed also advanced as a skill, however, routine checks always count and filling up any two of the three check categories advances Greed.

## Pass or Fail

Checks for advancement are earned whether the character was successful or not.  The exceptions are checks for Perception, Resources, and Faith, which require a successful check toward advancement.

## Series of Rolls

Occasionally in game, there are situations where players are testing the same ability over and over again&mdash;martial and social conflict being the prime examples.  In any instance where an ability is checked multiple times to determine the outcome, only one check is earned towards advancement.  Any check may be used for advancement but typically the one with the highest DC counts.

## Advancement for Graduated Checks

Graduated Checks are always considered routine with a DC of 1 for advancement.

## Advancement for Versus Checks

For versus checks, the number of successes your opponent generates plus any disadvantage is used to determine the DC for advancement.

## Helping and Advancement

Helping dice add to the dice total for the acting character, making it harder to earn difficult or challenging tests for advancement.  However, the Helpers can learn as well.  The level of the check is the same as if they had rolled against the difficulty with their own ability, regardless of the number of dice rolls.  If your skill is Proficiency 2 and the DC was 5, the check counts as challenging for advancement.  This applies if you're helping with a skill or a stat.

## Practice

Characters can undertake a practice regimen to advance their skills.  Practice helps fill in the gaps in a character's active experience.  The drawback to practice is that it requires substantial in-game time investment.

Practice comes in many forms; exercising, studying, and even trying out your skills on your friends.  These tables list the practice cycles for the different categories.  The Cycle is the length of time that a character needs to practice in order earn one check.  Hours indicates the actual time per day that the character spends practicing during the cycle.

If a character maintains the daily time requirement for one practice cycle, they earn the listed check for that skill for the purposes of advancement.  You cannot practice the same skill more than once per cycle.

| Skill Category | Cycle| Routing | Difficult | Challenging |
|----------------|------|---------|-----------|-------------|
| Academic          | 6 months | 2 hours | 4 hours  | 8 hours  |
| Artisan           | 1 year   | 4 hours | 8 hours  | 12 hours |
| Artist            | 6 months | 3 hours | 6 hours  | 12 hours |
| Craftsman         | 1 year   | 3 hours | 8 hours  | 12 hours |
| Forester          | 6 months | 3 hours | 6 hours  | 12 hours |
| Martial           | 1 month  | 2 hours | 4 hours  | 8 hours  |
| Medicinal         | 1 year   | 4 hours | 8 hours  | 12 hours |
| Military          | 6 months | 2 hours | 4 hours  | 8 hours  |
| Musical           | 1 month  | 2 hours | 4 hours  | 8 hours  |
| Peasant           | 3 months | 1 hours | 4 hours  | 12 hours |
| Physical          | 1 month  | 2 hours | 4 hours  | 8 hours  |
| School of Thought | 6 months | 3 hours | 6 hours  | 12 hours |
| Seafaring         | 3 months | 2 hours | 4 hours  | 8 hours  |
| Social            | 1 month  | 2 hours | 4 hours  | 8 hours  |
| Sorcerous         | 1 year   | 5 hours | 10 hours | 15 hours |
| Special/Misc.     | 3 months | 3 hours | 6 hours  | 12 hours |

| Stat/Attribute |Cycle| Routing | Difficult | Challenging |
|----------------|------|---------|-----------|-------------|
| Will              | 1 year   | 4 hours | 8 hours  | 16 hours |
| Perception        | 6 months | 3 hours | 6 hours  | 12 hours |
| Agility           | 3 months | 2 hours | 4 hours  | 8 hours  |
| Speed             | 3 months | 3 hours | 6 hours  | 9 hours  |
| Power             | 1 month  | 2 hours | 4 hours  | 8 hours  |
| Forte             | 2 months | 4 hours | 8 hours  | 16 hours |
| Faith             | 1 year   | 5 hours | 10 hours | 20 hours |
| Steel             | 2 months | 1 hours | 3 hours  | 9 hours  |

### Maximum Practice

A character may only practice for a number of hours equal to three times their Will proficiency per day.  The most a character can practice in a day, no matter the Will proficiency, is 20 hours in a day.

### Using Practice in the Game

Sometimes a player will indicate that their character is undertaking some long task like wandering the countryside, working on the farm, or meditating in the hills.  These periods count as practice for applicable skills and abilities.

Any time a player indicates some action for their character that is cool but otherwise has no direct effect on driving the game forward, the in-game time should be logged toward practice for the applicable abilities.

## Learning New Skills

### Root Stats

Every skill has a stat (or combination of stats) on which it is based which is referred to as the **Root**.

### Aptitude

**Aptitude** is the number of checks a character needs in order to gain a new skill.  Aptitude is equal to 10 minus the *root stat*.  If there are more than two root stats, then the aptitude is the average of the root stats, rounded down.

### Beginner's Luck

When using Beginner's Luck, it is possible to learn new skills.  However, the DCs for advancement are factored slightly differently.  When making a check with the root stat, note the DC before doubling.  If the DC would count as routine (and thus not actually count) for advancing the stat, you note the check for learning the skill.  If the Beginner's Luck check, before doubling, would count as difficult or challenging for the stat, note a check for advancing the root stat.

### Learning the Skill

Once when you meet the Aptitude requirement, your character learns the skill.  The beginning Proficiency is half the root, or half the average of two roots, rounded down.