---
title: "Quirks"
---

## General Quirks
* **Adaptive** - copy another player's trait for the day.
* **Artistic** - gain advantage on checks where artistic talent or taste is relevant.
* **Attractive** - gain advantage in situations where good looks might be important.
* **Daredevil** - gain advantage on checks involving risky or dangerous stunts or maneuvers.
* **Fearless** - advantage on checks to avoid the effects of fear or intimidation.
* **Immunity** - gain advantage on checks to avoid poison or disease; medics gain advantage on medical checks when tending to you.
* **Keen Senses** - advantage when making perception checks or checks involving spotting clues, searching for items, or listening for sounds.
* **Quick Reflexes** - advantage on checks reacting quickly to unexpected situations, such as dodging attacks or avoiding traps.
* **Resourceful** - advantage when improvising with available resources to solve a problem.


Compassionate: Your operator is deeply empathetic and caring, gaining advantage to healing or support-related checks.
Greedy: Your operator is obsessed with wealth and possessions, gaining advantage to checks related to acquiring or haggling for riches.
Honest: Your operator always tells the truth, gaining advantage to Charisma checks related to honesty or integrity but suffering disadvantage to deception or lying.
Loner: Your operator prefers solitude and isolation, gaining advantage to Spot and Combat checks when operating alone but disadvantage when working in a team.
Mechanically inclined: Your operator has a natural aptitude for machines and devices, gaining advantage to checks related to repairing or tinkering with machinery.
Technophile: Your operator has a deep affinity for technology, gaining advantage to checks related to hacking, repairing, or interacting with computer systems.
Paranoid: Your operator is constantly on edge and suspicious of others, gaining advantage to Spot checks but suffering disadvantage to Charisma checks.
Perfectionist: Your operator has an obsession with perfection, gaining advantage to checks when meticulously crafting or inspecting things, but takes twice as long to perform such actions.
Pyromaniac: Your operator has an insatiable fascination with fire, gaining advantage to checks related to fire manipulation, combustion, or explosives.
Quick: Your operator is naturally fast and nimble, gaining advantage on initiative checks.
Stoic: Your operator remains calm and composed in all situations, gaining advantage to checks related to maintaining emotional control or resisting fear or panic.
Superstitious: Your operator believes in various superstitions or omens, gaining advantages or disadvantages based on adherence to these beliefs.

## Negative Quirks

Alcoholic: Your operator has a dependency on alcohol, suffering penalties to certain checks when sober or suffering withdrawal symptoms.
Allergic: Your operator is allergic to a specific substance or material, suffering penalties when exposed to it.
Claustrophobic: Your operator is afraid of enclosed spaces, suffering penalties to checks when in cramped environments.
Fearful: Your operator is easily frightened, suffering penalties to checks when facing their fear or in scary situations.
Forgetful: Your operator has a poor memory, suffering penalties to checks that require recall or memorization.
Tactless: Your operator lacks tact and diplomacy, suffering penalties to social interactions or negotiation checks.
Sleepwalker: Your operator sleepwalks, potentially getting into dangerous situations or revealing information while asleep.
Pacifist: Your operator refuses to use violence, suffering penalties to combat-related checks or when forced to engage in combat.
Envious: Your operator is consumed by envy and jealousy, suffering penalties to checks when others succeed or possess something they desire.
Reckless: Your operator tends to act impulsively and without thinking, suffering penalties to cautious or strategic actions.
