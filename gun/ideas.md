---
title: "Ideas"
---

## Class Idea List
* Brawler
* Commander
* Combat Medic
* Enforcer
* Explosive Expert
* Hacker
* Heavy Gunner
* Infiltrator
* Inventor
* Locksmith
* Mechanic
* Modder
* Negotiator
* Shepherd
* Sniper
* Survivalist
* Thug
* Toaster Expert
* Trader
* Trooper

* Scrounger
* Slayer
* Speaker
* Survivor