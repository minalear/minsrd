---
title: 'Skills'
---

## Combat Skills
| Skill | Ability | Description |
|-------|---------|-------------|
| Automatic Weapons | AGI | Proficiency at using conventional automatic firearms. |
| Big Guns | END | Proficiency with heavy machine guns, flamethrowers, and other oversized weapons. |
| Brawling | END | Proficiency at unarmed fighting, grappling, and using fist weapons. |
| Energy Weapons | PER | Proficiency with non-standard, energy based weaponry. |
| Melee | STR | Proficiency with bladed and blunt weapons. |
| Sniper Rifles | PER | Proficiency with sniper rifles. |
| Small Arms | AGI | Proficiency at using conventional small firearms such as handguns or shotguns. |

## General Skills
| Skill | Ability | Description |
|-------|---------|-------------|
| Athletics | STR/AGI | Apply trained speed, strength, or stamina in some feat of physical exertion. |
| Craft/Repair | INT/PER | Create and repair devices both simple and complex. |
| Disable Device | AGI/PER | Disabling mundane devices such as locks, traps, or sensors. |
| Explosives | INT/PER | Proficiency with explosives and bomb defusal. |
| Medicine | INT/PER | Employ medical and psychological treatment for the injured or disturbed. |
| Nerd Stuff | INT | Know facts about academic or scientific fields, operating advanced technology, or hacking computer systems. |
| Pilot | INT/PER | Piloting vehicles or ride large beasts. |
| Sneaky Shit | AGI | Move without drawing notice, disguising. Roll it for stealth, disguise, infiltration, manual legerdemain, pickpocketing, and the defeat of security measures. |
| Spot | INT/PER | Notice anomalies or interesting facts about your environment. Roll it for searching places, detecting ambushes, noticing things, and reading the emotional state of other people. |
| Survive | INT/PER | Obtain the basics of food, water, and shelter in hostile environments, along with avoiding their natural perils. |
| Work | ANY | This is a catch-all skill for professions not represented by other skills. Roll it to work at a particular profession, art, or trade. |

## Social Skills
| Skill | Ability | Description |
|-------|---------|-------------|
| Barter | CHA | Find what you need on the market and sell what you have. Roll it to sell or buy things, figure out where to purchase hard-to-get or illicit goods, deal with customs agents, or run a business. |
| Hard Ass | CHA/STR | Allows you to intimidate others to do what you want and throw them off guard. |
| Kiss Ass | CHA | Allows you to sweet-talk information out of others and bring them to your way of thinking.  |