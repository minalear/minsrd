---
title: "Scrounger"
---

## Class Traits 

* Prime Attributes - Intelligence or Dexterity
* Scrounger Class Skills - Combat/Primitive, Combat/Unarmed, Computer, Culture/Literacy, History, Instructor, Language, Navigation, Perception, Profession, Science, Security, Tech/Any, and Vehicle/Any
* Class Ability - Flawless Skill
  * You may use this ability once per game day before rolling a skill check on the Scrounger class list.  You will automatically succeed on the check unless you roll a natural 2 on the dice, given that the GM agrees that success was ever possible.

## Packages

### Adventuring Scrounger

This package reflects a role and training not provided by the options below. Select this choice if you wish to pick your own set of skills for your Scrounger.

**Skills**: Any five skills, at least two of which must be Scrounger class skills.

### Crafter

A few settlements are lucky enough to have an experienced technical craftsman in residence, a man or woman capable not only of fixing damaged equipment, but also able to profitably barter their skills to outsiders. These crafters know how to use their skills efficiently, and the wealth they bring to a settlement is greatly valued.

**Skills**: Business, Combat/Any, Persuade, Tech/Any, Tech/Postech, Vehicle/Any

### Mentor

Some Scroungers feel an obligation to teach others the mysteries of old world technology. Few have the necessary talents to fully comprehend the labyrinthine principles of science and the exacting physical precision of technological repair, but the mentor always knows a little something useful for any technical situation.

**Skills**: Combat/Any, Culture/Literacy, Computer, Instructor, Tech/Any, Tech/Postech

### Miltech

It’s an unfortunate fact of post-apocalyptic life that most enclaves esteem most highly that tech which is most useful for killing people. Miltechs are those honored scrapsmiths and artisans capable of fashioning the spears, blades, armor, and sometimes even firearms that are so precious to their people. Not a few venture far from home in search of salvage and the blueprints of new weaponry.

**Skills**: Combat/Any, Culture/Literacy, Science, Security, Tactics, Tech/Postech

### Restorationist

Some Scroungers are determined to restore what has been lost, They are not content to patch together makeshift tatters, but struggle to restore basic industry and genuine creative processes. Their insistence on starting from raw materials is unpopular among those who prefer the easy shortcut of scrap-built gear, but their persuasive manner and far-sighted vision can attract followers- if they live long enough to realize their dreams.

**Skills**: Business, Combat/Any, Culture/Literacy, Leadership, Persuasion, Tech/Postech

### Retriever

Scrounging is a lot easier when someone else has already gone to the trouble of finding what you were looking for. Retrievers excel at acquiring “pre-found” relics, slipping past security and guardsmen to abstract the goods from their former owner. Others are more honest in their work, reserving their talents for cracking the ancient defenses of the old world to get at the succulent technology within.

**Skills**: Combat/Any, Perception, Security, Tech/Postech, Stealth, Survival

### Scientist

Precious few genuine scientists remain in the wreckage of the New Earth, but a few Scroungers still demonstrate the right mix of reason and curiosity necessary to master the lost arts of Old Terra. Few have access to more than the bare necessities for their studies, and most are willing to run enormous risks to gain more of the Before’s lost wisdom.

**Skills**: Combat/Any, Computer, Culture/Literacy, Science, Tech/Any, Tech/Postech

### Wandering Tech

The depths of the wilderness call some Scroungers. The richest caches of lost tech can be found in the most inaccessible places, and remote settlements are usually glad to see a man or woman able to repair their relics and improve their local technology.

**Skills**: Combat/Any, Navigation, Perception, Tech/Postech, Survival, Vehicle/Any