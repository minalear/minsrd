---
title: "Survivor"
---

## Class Traits

* Prime Attributes - Intelligence or Constitution
* Survivor Class Skills - Athletics, Combat/Unarmed, Combat/Primitive, Navigation,
Perception, Profession/Any, Stealth, Survival, Tech/Medical, and Vehicle/Any.
* Class Ability - Hard to Kill
  * The first time you are reduced to zero hit points in a day, you immediately regain 1 hit point for each experience level you possess. Optionally, you may allow yourself to fall unconscious from the injury, in which case you will appear dead but will awaken ten minutes later with 1 hit point. This ability does not work against injuries that a human could not possibly survive.

## Packages

### Adventuring Survivor

This package reflects a role and training not provided by the options below. Select this choice if you wish to pick your own set of skills for your Survivor.

**Skills**: Any five skills, at least two of which must be Survivor class skills.

### Stranger With No Name

Some Survivors are just perpetually passing through, moving through the isolated towns and villages of the new world without leaving a trace behind them. It could be they’re looking for something, or looking to stay clear of something that follows. These strangers rarely make much impression on others, but precious little seems able to keep them from moving where they will.

**Skills**: Combat/Any, Culture/Traveler, Navigation, Stealth, Survival, Vehicle/Any

### Explorer

A hard-bitten adventurer with a mind to find what has been lost, an explorer seeks out the ancient ruins of the old world to liberate whatever wealth or secrets it might hide. These expeditions tend to be hard on the participants, and there are times when the explorer himself is the only one to make it back: half radioactive, all mangled, and clutching a king’s ransom in tech.

**Skills**: Combat/Any, History, Navigation, Perception, Security, Survival

### Healer

Whether a frontier sawbones, a tribal medicine man, or a disciple of the fading secrets of Old Terran medicine, these Survivors lend their strength to those in need. Healers tend to be deeply respected by villagers and other common folk, and they can usually rely on a bed and a meal almost anywhere- provided there’s any food to be had in the first place. A failure to cure the headman’s ailing daughter, however, can put the Survivor’s ability to maintain their own health to the test.

**Skills**: Combat/Any, Instructor, Navigation, Perception, Tech/Medical, Survival

### Last of the Breed

Countless enclaves perish each year under the spears of raiders, the teeth of badlands mutants, or the relentless grind of simple privation. The Survivor is the last of their kind, carrying on their shoulders the only remnants of their ways and history. They might carry on for the sake of revenge against the powers that destroyed their people, or they might dream of rebuilding what was so bitterly lost to them.

**Skills**: Any one skill, Artist, Combat/Any, Culture/Any, History, Survival

### Family Man // Family Woman

These Survivors are remarkable only for the ferocity of their devotion. Many adventurers seek glory and wealth for their own sake, but these men and women fight for their kindred. Their families often suffer under some burden that only they can hope to lift, or are relying on the Survivor to bring them prosperity. Even after their immediate problems are resolved, such Survivors often find themselves venturing out once more to stockpile just a few more resources or earn a few more allies. In the wastelands, no safety is ever really enough.

**Skills**: Any two skills, Combat/Any, Culture/Any, Navigation, Survival

### Teacher

The will to survive extends beyond resisting the daily threats of mutants, famine, and plague. It also means preserving the culture and knowledge of the past and teaching it to those who come after. Some Survivors work as traveling sages and teachers, imparting the wisdom of the past to those who need it to survive the future.

**Skills**: Combat/Any, Culture/Literacy, History, Instructor, Science, Survival

### Wild Man / Wild Woman

A wild man- or woman- dares to live in the very worst of the wastelands, those lethal rad zones or mutated badlands where even bandits and raiders don’t dare go. These men and women often relish the dangers of their homes, even though simple survival there requires knowledge and expertise even beyond the scope of hardened wasteland travelers.

**Skills**: Athletics, Combat/Any, Navigation, Tech/Medical, Stealth, Survival