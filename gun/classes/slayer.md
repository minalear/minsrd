---
title: "Slayer"
---

## Class Traits

* Prime Attributes - Strength or Dexterity
* Slayer Class SKills - Athletics, Combat/Any, Exosuit, Leadership, Perception, Profes-
sion/Any, Stealth, and Tactics.
* Class ability - Red hand
  * Once per fight, you may use this skill before rolling your attack. You will hit your target on anything but a natural 1 on the hit roll. This ability can only be used when you are trying to kill someone, and will not work with nonlethal attacks or mere trick shooting.

## Packages

### Adventuring Slayer

This package reflects a role and training not provided by the options below. Select this choice if you wish to pick your own set of skills for your Slayer.

**Skills**: Any five skills, at least two of which must be Slayer class skills.

### Barbarian

These are savage men and women, devoid of the gentler graces and owing everything to the strength of their arms and the fury of their blades. While a barbarian may not have been reared under the calming influence of Old Terran ways or cultural mores, they are superbly adapted for surviving the bloody challenges of a world as savage as they.

**Skills**: Athletics, Combat/Primitive, Combat/Unarmed, Navigation, Perception, Survival

### Beast Hunter

Every hunter dreads a confrontation with the savage, mutated predators of the wasteland, but the beast hunter actively seeks out such dangerous foes. The radioactive badlands are constantly spawning fresh horrors, and enclaves will pay well for men and women willing to track and eliminate such threats to their farmers and trappers.

**Skills**: Combat/Any, Culture/Any, Navigation, Perception, Stealth, Survival

### Executioner

Some legal problems can best be solved with a knife. In the small, close-knit tribes and enclaves of the New Earth, it can often be difficult to exact justice on a malefactor. Even when a community agrees that something must be done, ties of kinship can threaten to tear an enclave apart should one of them carry out the act. Executioners are those men and women trusted to solve the problem, ready to move on to the next village when too many grudges have accumulated.

**Skills**: Combat/Any, Combat/Unarmed, Culture/Criminal, Culture/Traveller, Navigation, Survival

### Gunslinger

Some enclaves hold their scarce functioning firearms in almost totemic regard, treating them as symbols of an older and better world. The gunslinger is a man or woman dedicated to mastering their use, sometimes even capable of building new guns and ammunition from a suitable supply of parts. The gunslinger’s services are valuable, and they are often found far from home in the service of those who have need of their lethal aim.

**Skills**: Combat/Projectile, Tech/Postech, Navigation, Perception, Survival, Vehicle/Any

### Noble Warrior

Some enclaves are wealthy and populous enough to afford nobility, however the particular caste is called. While they might be named “directors”, or “mayors” or “lords”, the brute practicalities of the New Earth ensure that almost all such elites are composed of trained and hardened warriors. Younger sons and daughters are often forced to seek their fortune far from home, by enemies if not by poverty.

**Skills**: Combat/Any, Culture/Any, Leadership, Steward, Tactics, Vehicle/Land

### Paladin

Some Slayers hold to a creed greater than themselves, whether a religion that inspires or a cause that fires them. These warriors often travel in pursuit of some worthy means to advance their beliefs, and it is not uncommon for them to stand as protectors of those enclaves or outcasts who lack a champion of their own.

**Skills**: Combat/Any, Leadership, Navigation, Persuasion, Religion, Survival

### Tribal Champion

The exemplar of his people’s martial ways, the tribal champion is a hardened veteran of the dangers of the wasteland. His brethren rely on him to lead them to victory in battle and to teach them the arts of war. These obligations can weigh heavier on a champion than the physical dangers of the battlefield..

**Skills**: Athletics, Combat/Any, Combat/Primitive, Instructor, Leadership, Tactics