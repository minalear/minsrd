---
title: "Speaker"
---

## Class Traits

* Prime Attributes - Wisdom or Charisma
* Speaker Class Skills - Artist, Bureaucracy, Business, Combat/Unarmed, Combat/Primitive, Culture/Any, Gambling, History, Instructor, Language, Leadership, Persuade, Profession, Religion, Steward, and Tactics.
* Class Ability - Speaker's Voice
  * Once per game day, you can automatically convince a single intelligent NPC to feel friendly or intimidated, assuming that result is in any way plausible under the circumstances. Swayed subjects will cooperate in ways that seem reasonable to them. It is not necessary to share a language with the target, though complex requests may require it. This suasion lasts until circumstances make a different attitude appropriate. Alternately, you may use this ability once per game day to reroll a failed skill check related to cultural awareness or interpersonal relations.

## Packages

### Adventuring Speaker
This package reflects a role and training not provided by the options below. Select this choice if you wish to pick your own set of skills for your Speaker.

**Skills**: Any five skills, at least two of which must be Speaker class skills.

### Chieftain
The dangers of the new world take their toll on weak communities, and some are scattered and ruined by them. A chieftain who still had a tribe to lead would likely be too busy to seek adventure, but not all chieftains still have a people. There are times when a chieftain outlives his or her tribe, and the leader of a band is left alone to avenge the dead or redeem those that still might live. Other Speakers aspire to a chieftain’s rule, and cultivate themselves in preparation for the day when they can press their case before their people.

**Skills**: Combat/Primitive, History, Leadership, Persuade, Steward, Survival

### Diplomat
Some enclaves are gifted with men and women unusually capable of cutting deals and negotiating agreements with their neighbors. These diplomats are often entrusted with important missions to neighboring settlements, and those that survive the experience are respected for their far-traveled insights. All the same, aspiring diplomats often find it wise to travel in the company of well-armed companions.

**Skills**: Combat/Any, Culture/Traveler, Language, Navigation, Persuasion, Survival

### Dynast
However humble their present circumstances, this Speaker dreams of glory. They aspire to found a lasting dynasty, or built some great enclave strong enough to withstand the gnawing teeth of time and the hungry wastes. Adventuring often promises the only possible way to acquire the glory and resources necessary to make their dream a reality.

**Skills**: Bureaucracy, Combat/Any, Culture/Any, Leadership, Persuasion, Steward

### Rogue
A liar, a knave, and a trickster, this Speaker excels at using their natural powers of charm to baffle and disarm a mark. Some such rogues use their talents for the good of their home enclave, tricking foes and turning them away before they can do harm. Others have a greater interest in their own personal profit.

**Skills**: Combat/Primitive, Culture/Criminal, Gambling, Persuade, Stealth, Security

### Prophet
Every cause needs its prophet, and this Speaker is dedicated to advancing some noble truth. The cause might be the union of a number of feuding enclaves, or the spread of some inspiring faith, or the simple need to band together against the constantly encroaching hardships of the wastelands. Most such prophets are largely equipped to persuade their listeners, but they’re also ready to defend the faith at need.

**Skills**: Combat/Primitive, Instructor, Leadership, Language, Persuade, Religion

### Trader
Scroungers might be best at finding and fixing the relics of the old world, but a Speaker has certain advantages in selling them. Traders cross the desolate wastelands bearing valuable goods, and are tough enough to make it through in one piece. They bargain hard for their goods, but often as not they’re the only ones who could possibly get through the badlands to sell them in the first place.

**Skills**: Business, Combat/Any, Culture/Literacy, Navigation, Survival, Vehicle/Any

### War Leader
Some Speakers are meant for more martial ends, men and women entrusted with the training and leading of warriors for an enclave. While these war leaders may not have the personal prowess of a Slayer, they excel at leading their troops fearlessly into battle and holding them together in the face of insurmountable odds.

**Skills**: Athletics, Combat/Any, Combat/Unarmed, Instructor, Leadership, Tactics