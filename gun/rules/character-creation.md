---
title: "Character Creation"
---

## Outline

1. Create a character concept
1. Determine ability scores
1. Choose character quirks
1. Choose a class
1. Calculate derived stats such as health

## Generating Ability Scores

The ability scores and their descriptions can be found [here](/gun/rules/core.md).

Each character receives a number of points to spend on increasing their basic attributes.  All attributes start at a base of 1 and you have 14 points to allocate between the seven ability scores.  No score can go below 1 or above 6.

## Quirks

Quirks are unique character traits that add flavor and depth to your operator.  Quirks represent various idiosyncrasies, habits, behaviors, or physical traits that make your operator stand out from others.  Quirks can provide bonuses or penalties to some aspects of gameplay and can help shape the personality, backstory, and playstyle of your character.

Players may choose two quirks from the [Quirks Table](/gun/quirks.md) when they begin the game.  In addition, players may choose a negative quirk to gain the ability to choose an additional positive quirk.