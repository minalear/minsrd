---
title: "Combat"
---

When a fight is simple, resolve it with a single check.  If the outcome is a foregone conclusion, just say what happens.  For more complex skirmishes, use these rules.

## Turns and Rounds

Combat is separated into **turns** and **rounds**.  Each character takes **one turn per round** unless otherwise specified.  A round ends when every character capable of taking a turn has taken a turn.

Turns represent activity, not the passing of time.  Even though characters act in a certain order, the turns in each round are assumed to occur at roughly the same time.

Players always get to act first.  When combat begins, the players agree on which player (or allied NPC) to take the first turn.  If the players can't agree on someone then the DM chooses.

Next, the DM chooses a hostile NPC to act, followed by a player (or allied NPC) nominated by the player who acted previously.  This is followed by another hostile NPC of the DM's choice, then another player, and so on, alternating between hostile and allied characters until every character has acted.  If all characters on one side have acted, the remaining characters take their turns in an order decided either by the GM or the players, as relevant.

When every character has taken a turn, the round ends and a new one begins.  The turn order continues to alternate, so if one side took the last turn in the previous round, the other side starts the new round.  This can result in hostile NPCs acting first in a new round.

## Actions

Each round of combat, you have a Standard Action, Move Action

Combat in GUN is slightly different than performing skill checks.  It is mechanically inspired by Warhammer 40k and Age of Sigmar, where you perform an attack sequence for the number of attacks your character can perform.  These are usually determined by skills and the type of equipment you're using.

## Attack Sequence

* Roll a d6 for each attack and compare it to your weapon's hit rating
* Roll a d6 for each successful attack and compare it to the weapon's wound rating
* Defender rolls a d6 for each successful wound and compares it to their armor rating

## Weapon Attacks

Some weaponry typically have different attack profiles that provide different attack profiles.  When making a ranged attack, choose one of the weapon profiles and perform your attack sequences.  If the attack requires more ammo than the gun currently has loaded, then you may only attack up to the amount of ammo left in the gun before being required to reload.

