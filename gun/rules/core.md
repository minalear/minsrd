---
title: "Core Mechanics"
---

## Abilities

**Ability Scores** represent the base attributes of a character.  They represent the raw talent and prowess of the character.  A character can have an ability score between the range of 1 to 6.

**Strength** \
Represents your character's physical power and constitution, affecting their ability to lift, carry, and perform tasks that require brute force.

**Perception** \
Represents your character's senses, awareness, and intuition, affecting their ability to perceive and react to their surroundings, notice details, and detect threats.

**Endurance** \
represents a character's physical and mental resilience, toughness, and stamina

**Charisma** \
Represents your character's social skills, presence, and persuasiveness, affecting their ability to interact with others, negotiate, and influence others.

**Intelligence** \
Represents your character's intelligence, knowledge, and problem-solving skills, affecting their ability to reason, analyze, and recall information.

**Agility** \
Represents your character's speed, dexterity, and reflexes, affecting their ability to move quickly, dodge, and perform acrobatics.

**Luck** \
Represents your character's luck, uncanniness to get out of deadly situations, and ability to overcome devastating odds.

## Checks

Whenever a character attempts an action with a chance of failure, they must perform a **Check**. To make a check, the player rolls a number of dice equal to their **Exponent**, which is determined by their ability score and skill level for the relevant action. The Exponent represents the character's level of proficiency and expertise in that area.

To determine the outcome of the Check, the player counts the number of successful dice rolls. A successful roll is typically any die that shows a result of 4, 5, or 6, though some conditions may change this. The **Difficulty Check (DC)** is the number of successes needed to succeed at the Check. If the player gets fewer successes, the Check fails, and the character may suffer consequences as determined by the Game Master.

### Advantage and Disadvantage

Sometimes checks may be advantaged or disadvantaged.  For each advantage on the check, the player adds an additional d6 to the dice pool.  For each disadvantage, the DC of the check increases by one.  Advantage and disadvantage does not cancel each other out.

### Aid

Others can aid in a skill check as long as they are able to contribute something significant.  When aiding another, reduce the DC of the check by one and both parties make a check against the new DC.  When aiding, you don't necessarily have to roll the same skill as the player you're helping.

### Saving Throws

One special kind of check is called a **Saving Throw** which typically is required in response to some outside event, like dodging a trap or resisting some poison.  These checks are made using your relevant ability score plus any other miscellaneous modifiers.

## Skills

Skills are the specialized abilities and expertise that your operator possesses, representing their training, experience, and proficiency in various areas.  Whether it's wielding weapons, hacking into systems, scavenging for resources, or negotiating with others, your operator's skills will play a pivotal role in their survival.

Operators can have skills ranging between 1 to 6 with 1 being basic competence and 6 being exceptional prowess.  The skill's level is referred to as their **Proficiency**.

For complex tasks, an Operator will automatically fail if they are not trained in the skill.

A complete list of skills can be found [here](/gun/skills.md).

## Conditions

Operators may suffer from some lingering effect that could impact their checks.  They may become drunk, poisoned, or suffer a broken limb.  These are called **Conditions** and can negatively impact checks.  Conditions come with a severity number (*Nauseous 3*).  Operators suffer disadvantage equal to their worst condition's severity to any check where it is an impediment.
