---
title: 'Minalear SRD'
---

This site is used to host the SRDs (System Reference Documents) of the various ttrpg systems that I use in my various games.

- [OpenSpace](/space/) - OSR hacked 2d6 style game pulling inspiration from Cepheus Engine, Stars Without Borders, and other similar systems.
- [FIN](/fin/) - WIP homebrew system created as a lightweight Pathfinder